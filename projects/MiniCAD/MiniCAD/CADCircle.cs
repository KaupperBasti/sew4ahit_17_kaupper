﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


namespace MiniCAD
{
    class CADCircle : CADShape
    {
        Point midPoint;
        float radius;

        public CADCircle(Color color, int lineWidth, Point midPoint, float radius)
            : base(color, lineWidth)
        {
            this.midPoint = midPoint;
            this.radius = radius;


            this.selectionMarkers.Add(new Rectangle(midPoint.X - (int)radius - selectionDistance, midPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(midPoint.X + (int)radius - selectionDistance, midPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(midPoint.X - selectionDistance, midPoint.Y - (int)radius - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(midPoint.X - selectionDistance, midPoint.Y + (int)radius - selectionDistance, selectionDistance * 2, selectionDistance * 2));
        }

        public override void Draw(Graphics g)
        {
            g.DrawEllipse(new Pen(this.color, lineWidth), this.midPoint.X - this.radius, this.midPoint.Y - this.radius, 2 * this.radius, 2 * this.radius);
            if (isSelected)
            {
                Pen p = new Pen(selectionColor, 1);
                foreach (Rectangle r in selectionMarkers)
                    g.DrawRectangle(p, r);
                p.Dispose();
            }
        }

        public override bool Selected(Point mouse)
        {
            return (isSelected = Math.Sqrt(Math.Pow(mouse.X - midPoint.X, 2) + Math.Pow(mouse.Y - midPoint.Y,2)) <= radius);
        }

        public override string GetSaveString()
        {
            return "CADCircle;" + "xMid=" + midPoint.X + ";yMid=" + midPoint.Y + ";radius="+radius+";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B + ";penWidth=" + lineWidth;
        }

        public Point GetMiddlePoint()
        {
            return midPoint;
        }

        public float GetRadius()
        {
            return radius;
        }

        public PointF GetMidPoint()
        {
            return midPoint;
        }
    }
}
