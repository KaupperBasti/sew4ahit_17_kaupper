﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MiniCAD
{
    class CADLine : CADShape
    {
        Point startPoint;
        Point endPoint;


        public CADLine(Color color, int lineWidth, Point startPoint, Point endPoint)
            : base(color, lineWidth)
        {
            this.startPoint = startPoint;
            this.endPoint = endPoint;

            this.selectionMarkers.Add(new Rectangle(startPoint.X - selectionDistance, startPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(endPoint.X - selectionDistance, endPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
        }

        public override void Draw(Graphics g)
        {
            g.DrawLine(new Pen(this.color, lineWidth), this.startPoint, this.endPoint);
            if (isSelected)
            {
                Pen p = new Pen(selectionColor, 1);
                foreach (Rectangle r in selectionMarkers)
                    g.DrawRectangle(p, r);
                p.Dispose();
        }}

        public override bool Selected(Point mouse)
        {
            double alpha = Math.PI - Math.Atan(Math.Abs((double)(this.startPoint.Y - this.endPoint.Y))/Math.Abs((double)(this.startPoint.X - this.endPoint.X)));
            double beta = Math.PI - Math.Atan(Math.Abs((double)(this.startPoint.Y - mouse.Y)) / Math.Abs((double)(this.startPoint.X - mouse.X)));
            double gamma = alpha - beta;

            double startToMouse = Math.Sqrt(Math.Pow(Math.Abs(this.startPoint.X - mouse.X),2) + Math.Pow(Math.Abs(this.startPoint.Y - mouse.Y),2));
            double distance = Math.Abs(Math.Sin(gamma) * startToMouse);
            return (isSelected = distance <= this.selectionDistance/2);
        }

        public override string GetSaveString()
        {
            return "CADLine;" + "xStart=" + startPoint.X + ";yStart=" + startPoint.Y + ";xEnd=" + endPoint.X + ";yEnd=" + endPoint.Y + ";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B+";penWidth="+lineWidth;
        }


        public Point GetStartPoint()
        {
            return startPoint;
        }

        public Point GetEndPoint()
        {
            return endPoint;
        }

    }
}
