﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


namespace MiniCAD
{
    class CADRectangle : CADShape
    {
        Point topLeftPoint;
        Point bottomRightPoint;

        public CADRectangle(Color color, int lineWidth, Point topLeftPoint, Point bottomRightPoint)
            : base(color, lineWidth)
        {
            this.topLeftPoint = topLeftPoint;
            this.bottomRightPoint = bottomRightPoint;

            this.selectionMarkers.Add(new Rectangle(topLeftPoint.X - selectionDistance, topLeftPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(topLeftPoint.X - selectionDistance, bottomRightPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(bottomRightPoint.X - selectionDistance, topLeftPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2));
            this.selectionMarkers.Add(new Rectangle(bottomRightPoint.X - selectionDistance, bottomRightPoint.Y - selectionDistance, selectionDistance * 2, selectionDistance * 2)); 
        }

        public override void Draw(Graphics g)
        {
            g.DrawRectangle(new Pen(this.color, lineWidth), this.topLeftPoint.X, this.topLeftPoint.Y, this.bottomRightPoint.X - this.topLeftPoint.X, this.bottomRightPoint.Y - this.topLeftPoint.Y);
            if (isSelected)
            {
                Pen p = new Pen(selectionColor, 1);
                foreach (Rectangle r in selectionMarkers)
                    g.DrawRectangle(p, r);
                p.Dispose();
            }
        }

        public override bool Selected(Point mouse)
        {
            return (isSelected = (mouse.X > this.topLeftPoint.X && mouse.X < this.bottomRightPoint.X) && (mouse.Y > this.topLeftPoint.Y && mouse.Y < this.bottomRightPoint.Y));
        }

        public override string GetSaveString()
        {
            return "CADRectangle;" + "xTop=" + topLeftPoint.X + ";yTop=" + topLeftPoint.Y + ";xBottom=" + bottomRightPoint.X + ";yBottom=" + bottomRightPoint.Y + ";colorR=" + color.R + ";colorG=" + color.G + ";colorB=" + color.B + ";penWidth=" + lineWidth;
        }


        public Point GetTopLeftPoint()
        {
            return topLeftPoint;
        }

        public Point GetBottomRightPoint()
        {
            return bottomRightPoint;
        }



    }
}
