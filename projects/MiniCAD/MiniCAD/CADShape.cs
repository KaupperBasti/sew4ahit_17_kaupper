﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;


namespace MiniCAD
{
    interface IDrawAble 
    {
        void Draw(Graphics g);
    }

    interface ISelectAble
    {
        bool Selected(Point mouse);
        void SetSelection(bool selected);
    }

    interface ISaveAble
    {
        string GetSaveString();
    }

    interface IIntersectAble
    {
        PointF[] GetIntersections(CADShape s, CADShape s2);
    }

    abstract class CADShape : IDrawAble, ISelectAble, ISaveAble
    {
        protected Color color;
        protected int lineWidth;

        protected int selectionDistance;
        protected List<Rectangle> selectionMarkers;
        protected bool isSelected;
        protected Color selectionColor;

        public CADShape(Color color, int lineWidth)
        {
            this.color = color;
            this.lineWidth = lineWidth;
            this.selectionMarkers = new List<Rectangle>();
            this.selectionDistance = 5;
            this.isSelected = false;
            this.selectionColor = Color.Red;
        }

        public abstract void Draw(Graphics g);
        public abstract bool Selected(Point mouse);

        public void SetSelection(bool selected)
        {
            this.isSelected = selected;
        }

        public abstract string GetSaveString();
        




        public static CADShape LoadFromString(string s)
        {
            List<string> props = s.Split(';').ToList();
            CADShape shape = null;
            if (!props[0].StartsWith("CAD"))
                return null;

            if (props[0] == "CADLine")
            {
                props.RemoveAt(0);
                Point startPoint = new Point();
                Point endPoint = new Point();
                byte r = 0, g = 0, b = 0;
                float width = 0;

                #region LineSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "xStart":
                            int xs;
                            if (!int.TryParse(buff, out xs))
                                MessageBox.Show("Invalid Argument: "+st);
                            startPoint.X = xs;
                            break;
                        case "yStart":
                            int ys;
                            if (!int.TryParse(buff, out ys))
                                MessageBox.Show("Invalid Argument: "+st);
                            startPoint.Y =ys;
                            break;
                        case "xEnd":
                            int xe;
                            if (!int.TryParse(buff, out xe))
                                MessageBox.Show("Invalid Argument: "+st);
                            endPoint.X = xe;
                            break;
                        case "yEnd":
                             int ye;
                            if (!int.TryParse(buff, out ye))
                                MessageBox.Show("Invalid Argument: "+st);
                            endPoint.Y =ye;
                            break;
                        case "colorR":
                            if(!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG": 
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                             if(!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                             if(!float.TryParse(buff, out width))
                                MessageBox.Show("Invalid Argument: " + st);
                             break;

                        default:
                            MessageBox.Show("Invalid Argument: "+st);
                            break;
                    }
                }
                #endregion

                shape = new CADLine(Color.FromArgb(r, g, b), (int)width, startPoint, endPoint);

            }
            else if (props[0] == "CADRectangle")
            {
                props.RemoveAt(0);
                Point topLeftPoint = new Point();
                Point bottomRightPoint = new Point();
                byte r = 0, g = 0, b = 0;
                float width = 0;

                #region RectSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "xTop":
                            int xs;
                            if (!int.TryParse(buff, out xs))
                                MessageBox.Show("Invalid Argument: " + st);
                            topLeftPoint.X = xs;
                            break;
                        case "yTop":
                            int ys;
                            if (!int.TryParse(buff, out ys))
                                MessageBox.Show("Invalid Argument: " + st);
                            topLeftPoint.Y = ys;
                            break;
                        case "xBottom":
                            int xe;
                            if (!int.TryParse(buff, out xe))
                                MessageBox.Show("Invalid Argument: " + st);
                            bottomRightPoint.X = xe;
                            break;
                        case "yBottom":
                            int ye;
                            if (!int.TryParse(buff, out ye))
                                MessageBox.Show("Invalid Argument: " + st);
                            bottomRightPoint.Y = ye;
                            break;
                        case "colorR":
                            if (!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG":
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                            if (!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                            if (!float.TryParse(buff, out width))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;

                        default:
                            MessageBox.Show("Invalid Argument: " + st);
                            break;
                    }
                }
                #endregion

                shape = new CADRectangle(Color.FromArgb(r, g, b), (int)width, topLeftPoint, bottomRightPoint);
            }
            else if (props[0] == "CADCircle")
            {
                props.RemoveAt(0);
                Point midPoint = new Point();
                byte r = 0, g = 0, b = 0;
                float radius = 0;
                float width = 0;

                #region CircleSwitch
                foreach (string st in props)
                {
                    string buff = st.Split('=')[1];
                    switch (st.Split('=')[0])
                    {
                        case "xMid":
                            int x;
                            if (!int.TryParse(buff, out x))
                                MessageBox.Show("Invalid Argument: " + st);
                            midPoint.X = x;
                            break;
                        case "yMid":
                            int y;
                            if (!int.TryParse(buff, out y))
                                MessageBox.Show("Invalid Argument: " + st);
                            midPoint.Y = y;
                            break;
                        case "radius":
                            if(!float.TryParse(buff, out radius))
                                MessageBox.Show("Invalid Argument: "+st);
                            break;
                        case "colorR":
                            if (!byte.TryParse(buff, out r))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorG":
                            if (!byte.TryParse(buff, out g))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "colorB":
                            if (!byte.TryParse(buff, out b))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;
                        case "penWidth":
                            if (!float.TryParse(buff, out width))
                                MessageBox.Show("Invalid Argument: " + st);
                            break;

                        default:
                            MessageBox.Show("Invalid Argument: " + st);
                            break;
                    }
                }
                #endregion

                shape = new CADCircle(Color.FromArgb(r, g, b), (int)width,midPoint, radius);
            }

            return shape;
        }



        public static PointF[] GetIntersections(CADShape shape1, CADShape shape2)
        {
            List<PointF> intersections = new List<PointF>();

            if (shape1.GetType() == typeof(CADCircle))
            {
                if (shape2.GetType() == typeof(CADCircle))
                {
                    PointF midPoint1, midPoint2;
                    double radius1, radius2;
                    double xDiff, yDiff;

                    midPoint1 = (shape1 as CADCircle).GetMidPoint();
                    radius1 = (shape1 as CADCircle).GetRadius();

                    midPoint2 = (shape2 as CADCircle).GetMidPoint();
                    radius2 = (shape2 as CADCircle).GetRadius();


                    xDiff = midPoint2.X - midPoint1.X;
                    yDiff = midPoint2.Y - midPoint1.Y;

                    double abs = Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
                    // Steigungswinkel: midP1 - midP2
                    double alpha = Math.PI - Math.Atan(yDiff / xDiff);
                    // Steigungswinkel: midP1 - S
                    double beta = Math.Acos((abs / 2) / radius1);

                    List<double> newX = new List<double>();
                    List<double> newY = new List<double>();

                    newX.Add(midPoint1.X + Math.Cos(alpha + beta) * radius1);
                    newY.Add(midPoint1.Y + Math.Sin(alpha + beta) * radius1);

                    newX.Add(midPoint1.X - Math.Cos(alpha + beta) * radius1);
                    newY.Add(midPoint1.Y - Math.Sin(alpha + beta) * radius1);


                    newX.Add(midPoint1.X - Math.Cos(alpha - beta) * radius1);
                    newY.Add(midPoint1.Y - Math.Sin(alpha - beta) * radius1);

                    newX.Add(midPoint1.X + Math.Cos(alpha - beta) * radius1);
                    newY.Add(midPoint1.Y + Math.Sin(alpha - beta) * radius1);

                    for (int i = newX.Count-1; i>=0; i--)
                    {
                        double xDiff1 = Math.Abs(newX[i] - midPoint1.X);
                        double xDiff2 = Math.Abs(newX[i] - midPoint2.X);
                        
                        double yDiff1 = Math.Abs(newY[i] - midPoint1.Y);
                        double yDiff2 = Math.Abs(newY[i] - midPoint2.Y);

                        if (Math.Sqrt((yDiff1 * yDiff1) + (xDiff1 * xDiff1)) > radius2)
                            goto deleteIndex;
                        if (Math.Sqrt((yDiff2 * yDiff2) + (xDiff2 * xDiff2)) > radius1)
                            goto deleteIndex;
                        if (double.IsNaN(newX[i]) || double.IsNaN(newY[i]))
                            goto deleteIndex;

                        continue;
                    deleteIndex:
                        newX.RemoveAt(i);
                        newY.RemoveAt(i);
                        continue;
                    }

                    for (int i = 0; i < newX.Count; i++)
                        intersections.Add(new PointF((int)newX[i], (int)newY[i]));
                }
                else if (shape2.GetType() == typeof(CADRectangle))
                {
                    CADRectangle rect = shape2 as CADRectangle;
                    CADCircle circle = shape1 as CADCircle;

                    double xBuff, yBuff;
                    List<double> newX = new List<double>();

                    List<double> newY = new List<double>();

                    newX.Add(xBuff = rect.GetTopLeftPoint().X);
                    newY.Add(circle.GetMiddlePoint().Y - Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(xBuff - circle.GetMiddlePoint().X, 2)));

                    newX.Add(xBuff = rect.GetTopLeftPoint().X);
                    newY.Add(circle.GetMiddlePoint().Y + Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(xBuff - circle.GetMiddlePoint().X, 2)));

                    newX.Add(xBuff = rect.GetBottomRightPoint().X);
                    newY.Add(circle.GetMiddlePoint().Y - Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(xBuff - circle.GetMiddlePoint().X, 2)));

                    newX.Add(xBuff = rect.GetBottomRightPoint().X);
                    newY.Add(circle.GetMiddlePoint().Y + Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(xBuff - circle.GetMiddlePoint().X, 2)));

                    newY.Add(yBuff = rect.GetTopLeftPoint().Y);
                    newX.Add(circle.GetMiddlePoint().X - Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(yBuff - circle.GetMiddlePoint().Y, 2)));

                    newY.Add(yBuff = rect.GetTopLeftPoint().Y);
                    newX.Add(circle.GetMiddlePoint().X + Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(yBuff - circle.GetMiddlePoint().Y, 2)));

                    newY.Add(yBuff = rect.GetBottomRightPoint().Y);
                    newX.Add(circle.GetMiddlePoint().X - Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(yBuff - circle.GetMiddlePoint().Y, 2)));

                    newY.Add(yBuff = rect.GetBottomRightPoint().Y);
                    newX.Add(circle.GetMiddlePoint().X + Math.Sqrt(Math.Pow(circle.GetRadius(), 2) - Math.Pow(yBuff - circle.GetMiddlePoint().Y, 2)));


                    for (int i = newX.Count - 1; i >= 0; i--)
                    {
                        if (newX[i] == double.NaN || newY[i] == double.NaN)
                            goto deleteIndex;
                        if (newX[i] > circle.GetMiddlePoint().X + circle.GetRadius() || newX[i] < circle.GetMiddlePoint().X - circle.GetRadius())
                            goto deleteIndex;
                        if (newX[i] > rect.GetTopLeftPoint().X && newX[i] > rect.GetBottomRightPoint().X)
                            goto deleteIndex;
                        if (newX[i] < rect.GetTopLeftPoint().X && newX[i] < rect.GetBottomRightPoint().X)
                            goto deleteIndex;
                        if (newY[i] > circle.GetMiddlePoint().Y + circle.GetRadius() || newY[i] < circle.GetMiddlePoint().Y - circle.GetRadius())
                            goto deleteIndex;
                        if (newY[i] > rect.GetTopLeftPoint().Y && newY[i] > rect.GetBottomRightPoint().Y)
                            goto deleteIndex;
                        if (newY[i] < rect.GetTopLeftPoint().Y && newY[i] < rect.GetBottomRightPoint().Y)
                            goto deleteIndex;

                        continue;

                    deleteIndex:
                        newX.RemoveAt(i);
                        newY.RemoveAt(i);
                        continue;
                    }

                    for (int i = 0; i < newX.Count; i++)
                        intersections.Add(new PointF((float)newX[i], (float)newY[i]));
                }
                else if (shape2.GetType() == typeof(CADLine))
                {
                    CADCircle circle = shape1 as CADCircle;
                    CADLine line = shape2 as CADLine;

                    List<double> newX = new List<double>();
                    List<double> newY = new List<double>();

                    // Steigung der Geraden
                    double k = (double)(line.GetEndPoint().Y - line.GetStartPoint().Y) / (line.GetEndPoint().X - line.GetStartPoint().X);
                    // d Komponente der Gerade
                    double d = (double)line.GetStartPoint().Y - k * (double)line.GetStartPoint().X;

                    // neue 'd' Komponente der Geraden. Verschieben des Kreismittelpunktes durch den Ursprung
                    double y = k * (double)circle.GetMiddlePoint().X + d - (double)circle.GetMiddlePoint().Y;
                    double r = (double)circle.GetRadius();

                    // Gleichsetzen der Formeln y=kx+d und x²+y²=r² umgeformt nach x
                    newX.Add((double)(-y * k + Math.Sqrt(-(y * y) + r * r + r * r * k * k)) / (1.0 + k * k) + (double)circle.GetMiddlePoint().X);
                    newX.Add((-y * k - Math.Sqrt(-(y * y) + r * r + r * r * k * k)) / (1.0 + k * k) + (double)circle.GetMiddlePoint().X);

                    // Gleichsetzen der Formeln y=kx+d und x²+y²=r² umgeformt nach x
                    newY.Add((y + Math.Sqrt(k * k * r * r - k * k * y * y + k * k * k * k * r * r)) / (1.0 + k * k) + (double)circle.GetMiddlePoint().Y);
                    newY.Add((y - Math.Sqrt(k * k * r * r - k * k * y * y + k * k * k * k * r * r)) / (1.0 + k * k) + (double)circle.GetMiddlePoint().Y);


                    for (int i = newX.Count - 1; i >= 0; i--)
                    {
                        if (newX[i] > line.GetStartPoint().X && newX[i] > line.GetEndPoint().X)
                            goto deleteIndex;
                        if (newX[i] < line.GetStartPoint().X && newX[i] < line.GetEndPoint().X)
                            goto deleteIndex;
                        if (double.IsNaN(newX[i]) || double.IsNaN(newY[i]))
                            goto deleteIndex;

                        continue;

                    deleteIndex:
                        newX.RemoveAt(i);
                        newY.RemoveAt(i);
                        continue;
                    }

                    for (int i = 0; i < newX.Count; i++)
                        intersections.Add(new PointF((float)newX[i], (float)newY[i]));
                }
            }
            else if (shape1.GetType() == typeof(CADRectangle))
            {
                if (shape2.GetType() == typeof(CADCircle))
                    return CADShape.GetIntersections(shape2, shape1);
                else if (shape2.GetType() == typeof(CADRectangle))
                {
                    CADRectangle rect1 = shape1 as CADRectangle;
                    CADRectangle rect2 = shape2 as CADRectangle;

                    List<PointF> test1 = GetIntersections(rect1, new CADLine(rect2.GetColor(), rect2.GetLineWidth(), rect2.GetTopLeftPoint(), new Point(rect2.GetBottomRightPoint().X, rect2.GetTopLeftPoint().Y))).ToList();
                    List<PointF> test2 = GetIntersections(rect1, new CADLine(rect2.GetColor(), rect2.GetLineWidth(), rect2.GetTopLeftPoint(), new Point(rect2.GetTopLeftPoint().X, rect2.GetBottomRightPoint().Y))).ToList();
                    List<PointF> test3 = GetIntersections(rect1, new CADLine(rect2.GetColor(), rect2.GetLineWidth(), rect2.GetBottomRightPoint(), new Point(rect2.GetBottomRightPoint().X, rect2.GetTopLeftPoint().Y))).ToList();
                    List<PointF> test4 = GetIntersections(rect1, new CADLine(rect2.GetColor(), rect2.GetLineWidth(), rect2.GetBottomRightPoint(), new Point(rect2.GetTopLeftPoint().X, rect2.GetBottomRightPoint().Y))).ToList();

                    intersections.AddRange(test1.Concat(test2.Concat(test3.Concat(test4))).ToArray());
                }
                else if (shape2.GetType() == typeof(CADLine))
                {
                    CADRectangle rect = shape1 as CADRectangle;
                    CADLine line = shape2 as CADLine;

                    List<PointF> test1 = GetIntersections(line, new CADLine(rect.GetColor(), rect.GetLineWidth(), rect.GetTopLeftPoint(), new Point(rect.GetBottomRightPoint().X, rect.GetTopLeftPoint().Y))).ToList();
                    List<PointF> test2 = GetIntersections(line, new CADLine(rect.GetColor(), rect.GetLineWidth(), rect.GetTopLeftPoint(), new Point(rect.GetTopLeftPoint().X, rect.GetBottomRightPoint().Y))).ToList();
                    List<PointF> test3 = GetIntersections(line, new CADLine(rect.GetColor(), rect.GetLineWidth(), rect.GetBottomRightPoint(), new Point(rect.GetBottomRightPoint().X, rect.GetTopLeftPoint().Y))).ToList();
                    List<PointF> test4 = GetIntersections(line, new CADLine(rect.GetColor(), rect.GetLineWidth(), rect.GetBottomRightPoint(), new Point(rect.GetTopLeftPoint().X, rect.GetBottomRightPoint().Y))).ToList();

                    intersections.AddRange(test1.Concat(test2.Concat(test3.Concat(test4))).ToArray());
                }

            }
            else if (shape1.GetType() == typeof(CADLine))
            {
                if (shape2.GetType() != typeof(CADLine))
                    return CADShape.GetIntersections(shape2, shape1);
                else if (shape2.GetType() == typeof(CADLine))
                {
                    CADLine line1 = shape1 as CADLine;
                    CADLine line2 = shape2 as CADLine;

                    double k1 = (double)(line1.GetEndPoint().Y - line1.GetStartPoint().Y) / (line1.GetEndPoint().X - line1.GetStartPoint().X);
                    double d1 = (double)line1.GetStartPoint().Y - k1 * (double)line1.GetStartPoint().X;

                    double k2 = (double)(line2.GetEndPoint().Y - line2.GetStartPoint().Y) / (line2.GetEndPoint().X - line2.GetStartPoint().X);
                    double d2 = (double)line2.GetStartPoint().Y - k2 * (double)line2.GetStartPoint().X;


                    double newX = (d1 - d2) / (k2 - k1);
                    if (line1.GetEndPoint().X == line1.GetStartPoint().X)
                        newX = line1.GetEndPoint().X;
                    else if (line2.GetEndPoint().X == line2.GetStartPoint().X)
                        newX = line2.GetEndPoint().X;

                    double newY = (double.IsInfinity(k2) ? k1 : k2) * newX + (double.IsInfinity(d2) ? d1 : d2);


                    intersections.Add(new PointF((float)newX, (float)newY));

                    if (newX > line1.GetStartPoint().X && newX > line1.GetEndPoint().X)
                        intersections.Clear();
                    if (newX < line1.GetStartPoint().X && newX < line1.GetEndPoint().X)
                        intersections.Clear();
                    if (newX > line2.GetStartPoint().X && newX > line2.GetEndPoint().X)
                        intersections.Clear();
                    if (newX < line2.GetStartPoint().X && newX < line2.GetEndPoint().X)
                        intersections.Clear();
                    if (newY > line1.GetStartPoint().Y && newY > line1.GetEndPoint().Y)
                        intersections.Clear();
                    if (newY < line1.GetStartPoint().Y && newY < line1.GetEndPoint().Y)
                        intersections.Clear();
                    if (newY > line2.GetStartPoint().Y && newY > line2.GetEndPoint().Y)
                        intersections.Clear();
                    if (newY < line2.GetStartPoint().Y && newY < line2.GetEndPoint().Y)
                        intersections.Clear();
                    if (double.IsNaN(newX) || double.IsNaN(newY) || double.IsInfinity(newX) || double.IsInfinity(newY))
                        intersections.Clear();
                }
            }
            PointF[] temp = new PointF[intersections.Count];
            intersections.CopyTo(temp);
            intersections.Clear();
            for (int i = temp.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < intersections.Count; j++)
                    if (intersections[j].X == temp[i].X && intersections[j].Y == temp[i].Y)
                        goto continueOuterLoop;

                intersections.Add(temp[i]);
            continueOuterLoop: ;
            }

            return intersections.ToArray();
        }
        public static List<PointF> GetIntersections(List<CADShape> shapes)
        {
            List<PointF> intersections = new List<PointF>();
            
            for(int i = 0; i <shapes.Count;i++)
                for (int j = i + 1; j < shapes.Count; j++)
                    intersections.AddRange(GetIntersections(shapes[i], shapes[j]));

            return intersections;
        }

        public static List<PointF> GetIntersections(CADShape shape, List<CADShape> shapes)
        {
            List<PointF> intersections = new List<PointF>();

            foreach (CADShape s in shapes)
                intersections.AddRange(GetIntersections(shape, s));

            return intersections;
        }


        public Color GetColor()
        {
            return color;
        }

        public int GetLineWidth()
        {
            return lineWidth;
        }
    }
}
