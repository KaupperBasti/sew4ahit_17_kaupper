﻿namespace MiniCAD
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.shapeListBox = new System.Windows.Forms.ListBox();
            this.linePanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lineY2Box = new System.Windows.Forms.MaskedTextBox();
            this.lineY1Box = new System.Windows.Forms.MaskedTextBox();
            this.lineX2Box = new System.Windows.Forms.MaskedTextBox();
            this.lineX1Box = new System.Windows.Forms.MaskedTextBox();
            this.lineLabel = new System.Windows.Forms.Label();
            this.drawButton = new System.Windows.Forms.Button();
            this.drawingPanel = new System.Windows.Forms.Panel();
            this.descriptionYLabel = new System.Windows.Forms.Label();
            this.descriptionXLabel = new System.Windows.Forms.Label();
            this.description0Label = new System.Windows.Forms.Label();
            this.rectPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.rectY2Box = new System.Windows.Forms.MaskedTextBox();
            this.rectY1Box = new System.Windows.Forms.MaskedTextBox();
            this.rectX2Box = new System.Windows.Forms.MaskedTextBox();
            this.rectX1Box = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.circlePanel = new System.Windows.Forms.Panel();
            this.circleRadiusBox = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.circleYBox = new System.Windows.Forms.MaskedTextBox();
            this.circleXBox = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.zoomInButton = new System.Windows.Forms.Button();
            this.zoomOutButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label11 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.label15 = new System.Windows.Forms.Label();
            this.penWidth = new System.Windows.Forms.MaskedTextBox();
            this.penColor = new System.Windows.Forms.Panel();
            this.fileNameStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.linePanel.SuspendLayout();
            this.drawingPanel.SuspendLayout();
            this.rectPanel.SuspendLayout();
            this.circlePanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // shapeListBox
            // 
            this.shapeListBox.FormattingEnabled = true;
            this.shapeListBox.Items.AddRange(new object[] {
            "Line",
            "Rectangle",
            "Circle"});
            this.shapeListBox.Location = new System.Drawing.Point(12, 39);
            this.shapeListBox.Name = "shapeListBox";
            this.shapeListBox.Size = new System.Drawing.Size(181, 56);
            this.shapeListBox.TabIndex = 0;
            this.shapeListBox.SelectedIndexChanged += new System.EventHandler(this.shapeListBox_SelectedIndexChanged);
            // 
            // linePanel
            // 
            this.linePanel.Controls.Add(this.label4);
            this.linePanel.Controls.Add(this.label3);
            this.linePanel.Controls.Add(this.label2);
            this.linePanel.Controls.Add(this.label1);
            this.linePanel.Controls.Add(this.lineY2Box);
            this.linePanel.Controls.Add(this.lineY1Box);
            this.linePanel.Controls.Add(this.lineX2Box);
            this.linePanel.Controls.Add(this.lineX1Box);
            this.linePanel.Controls.Add(this.lineLabel);
            this.linePanel.Location = new System.Drawing.Point(12, 161);
            this.linePanel.Name = "linePanel";
            this.linePanel.Size = new System.Drawing.Size(181, 100);
            this.linePanel.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(96, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "y2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "x2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "y1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "x1";
            // 
            // lineY2Box
            // 
            this.lineY2Box.Location = new System.Drawing.Point(120, 63);
            this.lineY2Box.Mask = "9999px";
            this.lineY2Box.Name = "lineY2Box";
            this.lineY2Box.Size = new System.Drawing.Size(43, 20);
            this.lineY2Box.TabIndex = 4;
            // 
            // lineY1Box
            // 
            this.lineY1Box.Location = new System.Drawing.Point(120, 37);
            this.lineY1Box.Mask = "9999px";
            this.lineY1Box.Name = "lineY1Box";
            this.lineY1Box.Size = new System.Drawing.Size(43, 20);
            this.lineY1Box.TabIndex = 2;
            // 
            // lineX2Box
            // 
            this.lineX2Box.Location = new System.Drawing.Point(41, 63);
            this.lineX2Box.Mask = "9999px";
            this.lineX2Box.Name = "lineX2Box";
            this.lineX2Box.Size = new System.Drawing.Size(43, 20);
            this.lineX2Box.TabIndex = 3;
            // 
            // lineX1Box
            // 
            this.lineX1Box.Location = new System.Drawing.Point(41, 37);
            this.lineX1Box.Mask = "9999px";
            this.lineX1Box.Name = "lineX1Box";
            this.lineX1Box.Size = new System.Drawing.Size(43, 20);
            this.lineX1Box.TabIndex = 1;
            // 
            // lineLabel
            // 
            this.lineLabel.AutoSize = true;
            this.lineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lineLabel.Location = new System.Drawing.Point(11, 14);
            this.lineLabel.Name = "lineLabel";
            this.lineLabel.Size = new System.Drawing.Size(37, 16);
            this.lineLabel.TabIndex = 0;
            this.lineLabel.Text = "Line";
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(12, 303);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(83, 23);
            this.drawButton.TabIndex = 100;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // drawingPanel
            // 
            this.drawingPanel.BackColor = System.Drawing.Color.White;
            this.drawingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.drawingPanel.Controls.Add(this.descriptionYLabel);
            this.drawingPanel.Controls.Add(this.descriptionXLabel);
            this.drawingPanel.Controls.Add(this.description0Label);
            this.drawingPanel.Cursor = System.Windows.Forms.Cursors.Cross;
            this.drawingPanel.Location = new System.Drawing.Point(199, 39);
            this.drawingPanel.Name = "drawingPanel";
            this.drawingPanel.Size = new System.Drawing.Size(431, 286);
            this.drawingPanel.TabIndex = 101;
            // 
            // descriptionYLabel
            // 
            this.descriptionYLabel.AutoSize = true;
            this.descriptionYLabel.BackColor = System.Drawing.Color.White;
            this.descriptionYLabel.Location = new System.Drawing.Point(0, 271);
            this.descriptionYLabel.Name = "descriptionYLabel";
            this.descriptionYLabel.Size = new System.Drawing.Size(25, 13);
            this.descriptionYLabel.TabIndex = 2;
            this.descriptionYLabel.Text = "100";
            // 
            // descriptionXLabel
            // 
            this.descriptionXLabel.AutoSize = true;
            this.descriptionXLabel.BackColor = System.Drawing.Color.White;
            this.descriptionXLabel.Location = new System.Drawing.Point(406, 0);
            this.descriptionXLabel.Name = "descriptionXLabel";
            this.descriptionXLabel.Size = new System.Drawing.Size(25, 13);
            this.descriptionXLabel.TabIndex = 1;
            this.descriptionXLabel.Text = "100";
            // 
            // description0Label
            // 
            this.description0Label.AutoSize = true;
            this.description0Label.BackColor = System.Drawing.Color.White;
            this.description0Label.Location = new System.Drawing.Point(0, 0);
            this.description0Label.Name = "description0Label";
            this.description0Label.Size = new System.Drawing.Size(13, 13);
            this.description0Label.TabIndex = 0;
            this.description0Label.Text = "0";
            // 
            // rectPanel
            // 
            this.rectPanel.Controls.Add(this.label5);
            this.rectPanel.Controls.Add(this.label6);
            this.rectPanel.Controls.Add(this.label7);
            this.rectPanel.Controls.Add(this.label8);
            this.rectPanel.Controls.Add(this.rectY2Box);
            this.rectPanel.Controls.Add(this.rectY1Box);
            this.rectPanel.Controls.Add(this.rectX2Box);
            this.rectPanel.Controls.Add(this.rectX1Box);
            this.rectPanel.Controls.Add(this.label9);
            this.rectPanel.Location = new System.Drawing.Point(12, 161);
            this.rectPanel.Name = "rectPanel";
            this.rectPanel.Size = new System.Drawing.Size(181, 100);
            this.rectPanel.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(96, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "y2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "x2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(96, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "y1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 40);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "x1";
            // 
            // rectY2Box
            // 
            this.rectY2Box.Location = new System.Drawing.Point(120, 63);
            this.rectY2Box.Mask = "9999px";
            this.rectY2Box.Name = "rectY2Box";
            this.rectY2Box.Size = new System.Drawing.Size(43, 20);
            this.rectY2Box.TabIndex = 8;
            // 
            // rectY1Box
            // 
            this.rectY1Box.Location = new System.Drawing.Point(120, 37);
            this.rectY1Box.Mask = "9999px";
            this.rectY1Box.Name = "rectY1Box";
            this.rectY1Box.Size = new System.Drawing.Size(43, 20);
            this.rectY1Box.TabIndex = 6;
            // 
            // rectX2Box
            // 
            this.rectX2Box.Location = new System.Drawing.Point(41, 63);
            this.rectX2Box.Mask = "9999px";
            this.rectX2Box.Name = "rectX2Box";
            this.rectX2Box.Size = new System.Drawing.Size(43, 20);
            this.rectX2Box.TabIndex = 7;
            // 
            // rectX1Box
            // 
            this.rectX1Box.Location = new System.Drawing.Point(41, 37);
            this.rectX1Box.Mask = "9999px";
            this.rectX1Box.Name = "rectX1Box";
            this.rectX1Box.Size = new System.Drawing.Size(43, 20);
            this.rectX1Box.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "Rectangle";
            // 
            // circlePanel
            // 
            this.circlePanel.Controls.Add(this.circleRadiusBox);
            this.circlePanel.Controls.Add(this.label10);
            this.circlePanel.Controls.Add(this.label12);
            this.circlePanel.Controls.Add(this.label13);
            this.circlePanel.Controls.Add(this.circleYBox);
            this.circlePanel.Controls.Add(this.circleXBox);
            this.circlePanel.Controls.Add(this.label14);
            this.circlePanel.Location = new System.Drawing.Point(12, 161);
            this.circlePanel.Name = "circlePanel";
            this.circlePanel.Size = new System.Drawing.Size(181, 100);
            this.circlePanel.TabIndex = 10;
            // 
            // circleRadiusBox
            // 
            this.circleRadiusBox.Location = new System.Drawing.Point(89, 63);
            this.circleRadiusBox.Mask = "9999px";
            this.circleRadiusBox.Name = "circleRadiusBox";
            this.circleRadiusBox.Size = new System.Drawing.Size(43, 20);
            this.circleRadiusBox.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(40, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Radius";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(96, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(12, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "y";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "x";
            // 
            // circleYBox
            // 
            this.circleYBox.Location = new System.Drawing.Point(120, 37);
            this.circleYBox.Mask = "9999px";
            this.circleYBox.Name = "circleYBox";
            this.circleYBox.Size = new System.Drawing.Size(43, 20);
            this.circleYBox.TabIndex = 10;
            // 
            // circleXBox
            // 
            this.circleXBox.Location = new System.Drawing.Point(41, 37);
            this.circleXBox.Mask = "9999px";
            this.circleXBox.Name = "circleXBox";
            this.circleXBox.Size = new System.Drawing.Size(43, 20);
            this.circleXBox.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(11, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 16);
            this.label14.TabIndex = 0;
            this.label14.Text = "Circle";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileNameStatusLabel,
            this.statusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 328);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(642, 22);
            this.statusStrip1.TabIndex = 102;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabel1
            // 
            this.statusLabel1.Name = "statusLabel1";
            this.statusLabel1.Size = new System.Drawing.Size(131, 17);
            this.statusLabel1.Text = "2014 Kaupper Sebastian";
            // 
            // zoomInButton
            // 
            this.zoomInButton.Location = new System.Drawing.Point(12, 274);
            this.zoomInButton.Name = "zoomInButton";
            this.zoomInButton.Size = new System.Drawing.Size(84, 23);
            this.zoomInButton.TabIndex = 98;
            this.zoomInButton.Text = "Zoom in";
            this.zoomInButton.UseVisualStyleBackColor = true;
            this.zoomInButton.Click += new System.EventHandler(this.zoomInButton_Click);
            // 
            // zoomOutButton
            // 
            this.zoomOutButton.Location = new System.Drawing.Point(111, 274);
            this.zoomOutButton.Name = "zoomOutButton";
            this.zoomOutButton.Size = new System.Drawing.Size(82, 23);
            this.zoomOutButton.TabIndex = 99;
            this.zoomOutButton.Text = "Zoom out";
            this.zoomOutButton.UseVisualStyleBackColor = true;
            this.zoomOutButton.Click += new System.EventHandler(this.zoomOutButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(111, 303);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(82, 23);
            this.deleteButton.TabIndex = 101;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(642, 24);
            this.menuStrip1.TabIndex = 103;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 108);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Pen Width:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 104;
            this.label15.Text = "Pen Color: ";
            // 
            // penWidth
            // 
            this.penWidth.Location = new System.Drawing.Point(150, 105);
            this.penWidth.Mask = "9999px";
            this.penWidth.Name = "penWidth";
            this.penWidth.Size = new System.Drawing.Size(43, 20);
            this.penWidth.TabIndex = 105;
            // 
            // penColor
            // 
            this.penColor.BackColor = System.Drawing.Color.Black;
            this.penColor.Location = new System.Drawing.Point(150, 131);
            this.penColor.Name = "penColor";
            this.penColor.Size = new System.Drawing.Size(43, 20);
            this.penColor.TabIndex = 106;
            this.penColor.Click += new System.EventHandler(this.penColor_Click);
            // 
            // fileNameStatusLabel
            // 
            this.fileNameStatusLabel.Name = "fileNameStatusLabel";
            this.fileNameStatusLabel.Size = new System.Drawing.Size(116, 17);
            this.fileNameStatusLabel.Text = "Unbekanntes Projekt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 350);
            this.Controls.Add(this.penColor);
            this.Controls.Add(this.penWidth);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.zoomOutButton);
            this.Controls.Add(this.zoomInButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.circlePanel);
            this.Controls.Add(this.rectPanel);
            this.Controls.Add(this.drawingPanel);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.linePanel);
            this.Controls.Add(this.shapeListBox);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "MiniCAD";
            this.linePanel.ResumeLayout(false);
            this.linePanel.PerformLayout();
            this.drawingPanel.ResumeLayout(false);
            this.drawingPanel.PerformLayout();
            this.rectPanel.ResumeLayout(false);
            this.rectPanel.PerformLayout();
            this.circlePanel.ResumeLayout(false);
            this.circlePanel.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox shapeListBox;
        private System.Windows.Forms.Panel linePanel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox lineY2Box;
        private System.Windows.Forms.MaskedTextBox lineY1Box;
        private System.Windows.Forms.MaskedTextBox lineX2Box;
        private System.Windows.Forms.MaskedTextBox lineX1Box;
        private System.Windows.Forms.Label lineLabel;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.Panel drawingPanel;
        private System.Windows.Forms.Label descriptionYLabel;
        private System.Windows.Forms.Label descriptionXLabel;
        private System.Windows.Forms.Label description0Label;
        private System.Windows.Forms.Panel rectPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox rectY2Box;
        private System.Windows.Forms.MaskedTextBox rectY1Box;
        private System.Windows.Forms.MaskedTextBox rectX2Box;
        private System.Windows.Forms.MaskedTextBox rectX1Box;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel circlePanel;
        private System.Windows.Forms.MaskedTextBox circleRadiusBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox circleYBox;
        private System.Windows.Forms.MaskedTextBox circleXBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel1;
        private System.Windows.Forms.Button zoomInButton;
        private System.Windows.Forms.Button zoomOutButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox penWidth;
        private System.Windows.Forms.Panel penColor;
        private System.Windows.Forms.ToolStripStatusLabel fileNameStatusLabel;
    }
}

