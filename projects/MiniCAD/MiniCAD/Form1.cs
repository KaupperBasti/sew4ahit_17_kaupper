﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;

namespace MiniCAD
{
    enum Shapes
    {
        Line = 0,
        Rectangle = 1,
        Circle = 2
    }

    public partial class Form1 : Form
    {
        double zoomFactor;

        CADShape clickedShape;

        Shapes selectedShape;
        List<CADShape> shapes;
        List<PointF> intersections;
        Color intersectionColor;
        float intersectionWidth;

        public Form1()
        {
            InitializeComponent();
            this.Icon = Ressourcen.logo2;

            this.shapes = new List<CADShape>();
            this.intersections = new List<PointF>();
            this.intersectionColor = Color.Red;
            this.intersectionWidth = 3;
            this.drawingPanel.Paint += new PaintEventHandler(drawingPanel_Paint);
            this.drawingPanel.MouseClick += new MouseEventHandler(drawingPanel_MouseClick);

            this.descriptionXLabel.Text = this.drawingPanel.Width.ToString();
            this.descriptionYLabel.Text = this.drawingPanel.Height.ToString();

            this.rectPanel.Hide();
            this.linePanel.Hide();
            this.circlePanel.Hide();

            this.shapeListBox.SelectedIndex = 0;
            this.zoomFactor = 1.0;
            this.clickedShape = null;
            this.deleteButton.Enabled = false;

            this.penWidth.Text = "1";
        }

        void showProperties(CADShape s)
        {
            Type t = s.GetType();

            if (t == typeof(CADLine))
            {
                CADLine l = (CADLine)s;
                shapeListBox.SelectedIndex = (int)Shapes.Line;
                lineX1Box.Text = l.GetStartPoint().X.ToString();
                lineY1Box.Text = l.GetStartPoint().Y.ToString();
                lineX2Box.Text = l.GetEndPoint().X.ToString();
                lineY2Box.Text = l.GetEndPoint().Y.ToString();
            }
            else if (t == typeof(CADCircle))
            {
                CADCircle c = (CADCircle)s;
                shapeListBox.SelectedIndex = (int)Shapes.Circle;
                circleXBox.Text = c.GetMiddlePoint().X.ToString();
                circleYBox.Text = c.GetMiddlePoint().Y.ToString();
                circleRadiusBox.Text = c.GetRadius().ToString();
            }
            else if (t == typeof(CADRectangle))
            {
                CADRectangle r = (CADRectangle)s;
                shapeListBox.SelectedIndex = (int)Shapes.Rectangle;
                rectX1Box.Text = r.GetTopLeftPoint().X.ToString();
                rectY1Box.Text = r.GetTopLeftPoint().Y.ToString();
                rectX2Box.Text = r.GetBottomRightPoint().X.ToString();
                rectY2Box.Text = r.GetBottomRightPoint().Y.ToString();
            }
            penWidth.Text = s.GetLineWidth().ToString();
            penColor.BackColor = s.GetColor();
        }

        void deSelectAllShapes()
        {
            foreach (CADShape s in shapes)
                s.SetSelection(false);
            deleteButton.Enabled = false;
        }

        void resetAllInputs()
        {
            foreach (Control c in linePanel.Controls)
                if (typeof(MaskedTextBox) == c.GetType())
                    c.Text = "";

            foreach (Control c in circlePanel.Controls)
                if (typeof(MaskedTextBox) == c.GetType())
                    c.Text = "";

            foreach (Control c in rectPanel.Controls)
                if (typeof(MaskedTextBox) == c.GetType())
                    c.Text = "";
        }


        void drawingPanel_MouseClick(object sender, MouseEventArgs e)
        {
            resetAllInputs();
            this.clickedShape = null;
            this.deleteButton.Enabled = false;
            this.intersections.Clear();
            foreach (CADShape s in shapes)
                s.SetSelection(false);

            foreach (CADShape s in this.shapes)
                if (s.Selected(new Point((int)(e.Location.X / zoomFactor), (int)(e.Location.Y /zoomFactor))))
                {
                    this.clickedShape = s;
                    showProperties(s);
                    this.deleteButton.Enabled = true;
                    s.SetSelection(true);
                    this.intersections = CADShape.GetIntersections(s, shapes.FindAll((cad)=>cad!=s));
                    break;
                }
            this.drawingPanel.Invalidate();
        }

        void drawingPanel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Transform = new Matrix((float)zoomFactor, 0, 0, (float)zoomFactor, 0, 0);

            foreach (CADShape s in this.shapes)
                s.Draw(e.Graphics);
            foreach (PointF p in this.intersections)
            {
                e.Graphics.FillEllipse(new SolidBrush(intersectionColor), p.X - intersectionWidth, p.Y - intersectionWidth, 2 * intersectionWidth, 2 * intersectionWidth);
                e.Graphics.DrawString("(" + Math.Round(p.X, 2) + "," + Math.Round(p.Y, 2) + ")", SystemFonts.DefaultFont, Brushes.Black, p.X + intersectionWidth, p.Y - intersectionWidth);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            this.shapes.Remove(this.clickedShape);

            this.clickedShape = null;
            this.deleteButton.Enabled = false;

            this.intersections.Clear();
            this.drawingPanel.Invalidate();
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            Rectangle invalidateRect = new Rectangle(0,0,0,0);
            switch (this.selectedShape)
            {
                case Shapes.Line:
                    if (this.lineX1Box.Text.Replace("px", "").Trim() == "" ||
                        this.lineY1Box.Text.Replace("px", "").Trim() == "" ||
                        this.lineX2Box.Text.Replace("px", "").Trim() == "" ||
                        this.lineY2Box.Text.Replace("px", "").Trim() == "" ||
                        penWidth.Text.Replace("px", "").Trim() == "")
                    {
                        MessageBox.Show("Empty textboxes!");
                        break;
                    }

                    int lx1 = int.Parse(this.lineX1Box.Text.Replace("px", "").Trim());
                    int ly1 = int.Parse(this.lineY1Box.Text.Replace("px", "").Trim());
                    int lx2 = int.Parse(this.lineX2Box.Text.Replace("px", "").Trim());
                    int ly2 = int.Parse(this.lineY2Box.Text.Replace("px", "").Trim());

                    invalidateRect = new Rectangle(lx1, ly1, lx2 - lx1, ly2 - ly1);
                    this.shapes.Add(new CADLine(penColor.BackColor, int.Parse(penWidth.Text.Replace("px", "").Trim()), new Point(lx1, ly1), new Point(lx2, ly2)));
                    break;

                case Shapes.Rectangle:
                    if (this.rectX1Box.Text.Replace("px", "").Trim() == "" ||
                        this.rectY1Box.Text.Replace("px", "").Trim() == "" ||
                        this.rectX2Box.Text.Replace("px", "").Trim() == "" ||
                        this.rectY2Box.Text.Replace("px", "").Trim() == "" ||
                        penWidth.Text.Replace("px", "").Trim() == "")
                    {
                        MessageBox.Show("Empty textboxes!");
                        break;
                    }
                    
                    int rx1 = int.Parse(this.rectX1Box.Text.Replace("px", "").Trim());
                    int ry1 = int.Parse(this.rectY1Box.Text.Replace("px", "").Trim());
                    int rx2 = int.Parse(this.rectX2Box.Text.Replace("px", "").Trim());
                    int ry2 = int.Parse(this.rectY2Box.Text.Replace("px", "").Trim());

                    invalidateRect = new Rectangle(rx1 - int.Parse(penWidth.Text.Replace("px", "").Trim()) / 2, ry1 - int.Parse(penWidth.Text.Replace("px", "").Trim()) / 2, rx2 - rx1 + int.Parse(penWidth.Text.Replace("px", "").Trim()), ry2 - ry1 + int.Parse(penWidth.Text.Replace("px", "").Trim()));
                    this.shapes.Add(new CADRectangle(penColor.BackColor, int.Parse(penWidth.Text.Replace("px", "").Trim()), new Point(rx1, ry1), new Point(rx2, ry2)));
                    break;

                case Shapes.Circle:
                    if (this.circleXBox.Text.Replace("px", "").Trim() == "" ||
                        this.circleYBox.Text.Replace("px", "").Trim() == "" ||
                        this.circleRadiusBox.Text.Replace("px", "").Trim() == ""||
                        penWidth.Text.Replace("px", "").Trim() == "")
                    {
                        MessageBox.Show("Empty textboxes!");
                        break;
                    }

                    int x = int.Parse(this.circleXBox.Text.Replace("px", "").Trim());
                    int y = int.Parse(this.circleYBox.Text.Replace("px", "").Trim());
                    int r = int.Parse(this.circleRadiusBox.Text.Replace("px", "").Trim());

                    invalidateRect = new Rectangle(x - r - int.Parse(penWidth.Text.Replace("px", "").Trim()) / 2, y - r - int.Parse(penWidth.Text.Replace("px", "").Trim()) / 2, 2 * r + int.Parse(penWidth.Text.Replace("px", "").Trim()), 2 * r + int.Parse(penWidth.Text.Replace("px", "").Trim()));
                    this.shapes.Add(new CADCircle(penColor.BackColor, int.Parse(penWidth.Text.Replace("px", "").Trim()), new Point(x, y), r));
                    break;

                default:
                    MessageBox.Show("Unknown or no shape selected!");
                    break;
            }

            deSelectAllShapes();
            resetAllInputs();

            this.intersections.Clear();
            if (clickedShape != null)
            {
                this.shapes.Remove(clickedShape);
                (clickedShape = this.shapes[this.shapes.Count - 1]).SetSelection(true);
                showProperties(clickedShape);
                deleteButton.Enabled = true;
                this.intersections = CADShape.GetIntersections(clickedShape, shapes.FindAll((cad) => cad != clickedShape));
                InvalidateIntersectionPoints();
            }

            //this.intersections = CADShape.GetIntersections(shapes);
            //InvalidateIntersectionPoints();
            this.drawingPanel.Invalidate(invalidateRect);
        }

        private void InvalidateIntersectionPoints()
        {
            foreach (PointF p in intersections)
                drawingPanel.Invalidate(new Rectangle((int)(p.X - intersectionWidth / 2), (int)(p.Y - intersectionWidth / 2), (int)intersectionWidth, (int)intersectionWidth));
        }

        private void shapeListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            resetAllInputs();
            deSelectAllShapes();
            switch (this.shapeListBox.SelectedItem.ToString())
            {
                case "Line":
                    this.selectedShape = Shapes.Line;
                    this.rectPanel.Hide();
                    this.circlePanel.Hide();
                    this.linePanel.Show();
                    break;

                case "Rectangle":
                    this.selectedShape = Shapes.Rectangle;
                    this.linePanel.Hide();
                    this.circlePanel.Hide();
                    this.rectPanel.Show();
                    break;

                case "Circle":
                    this.selectedShape = Shapes.Circle;
                    this.rectPanel.Hide();
                    this.linePanel.Hide();
                    this.circlePanel.Show();
                    break;

                default:
                    MessageBox.Show("Unknown shape selected!");
                    break;
            }
            this.drawingPanel.Invalidate();
        }

        private void zoomInButton_Click(object sender, EventArgs e)
        {
            zoomFactor += 0.5;
            if (zoomFactor > 5)
                zoomFactor = 5;

            this.drawingPanel.Invalidate();
        }

        private void zoomOutButton_Click(object sender, EventArgs e)
        {
            zoomFactor -= 0.5;

            if (zoomFactor < 1)
                zoomFactor = 1;

            this.drawingPanel.Invalidate();
        }

        private void penColor_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
                penColor.BackColor = colorDialog1.Color;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            shapes.Clear();
            fileNameStatusLabel.Text = "Unbekanntes Projekt";
            drawingPanel.Invalidate();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sd = new SaveFileDialog();

            sd.Filter = "CAD-Datei|*.cad";

            if (fileNameStatusLabel.Text != "Unbekanntes Projekt")
                SaveProject(fileNameStatusLabel.Tag.ToString()); 
            else if (sd.ShowDialog() == DialogResult.OK)
                SaveProject(sd.FileName);       
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();

            od.Filter = "CAD-Datei|*.cad";

            if (od.ShowDialog() == DialogResult.OK)
                LoadProject(od.FileName);        }


        private void SaveProject(string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName, false);
            foreach (CADShape s in shapes)
                sw.WriteLine(s.GetSaveString() + "\n");
            
            sw.Close();

            fileNameStatusLabel.Text = Path.GetFileName(fileName);
        }

        private void LoadProject(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);
            shapes = new List<CADShape>();

            string buffer;
            while ((buffer = sr.ReadLine()) != null)
                shapes.Add(CADShape.LoadFromString(buffer));

            shapes.RemoveAll((c) => c == null);
            sr.Close();

            drawingPanel.Invalidate();
            fileNameStatusLabel.Text = Path.GetFileName(fileName);
            fileNameStatusLabel.Tag = fileName;
        }
    }
}
