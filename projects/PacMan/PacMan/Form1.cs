﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PacMan
{
    public partial class Form1 : Form
    {
        PacManPanel panel;
        PacMan pacMan;

        public Form1()
        {
            InitializeComponent();

            pacmanStepTimer.Stop();

            panel = new PacManPanel();
            panel.Dock = DockStyle.Fill;
            this.Controls.Add(panel);

            panel.AddPacMan(pacMan = new PacMan(Color.Red, 100, new Point(100, 100), 3, Direction.North, panel.Size));
            KeyDown += (s,e)=>panel.OnKeyDown(e);

            pacmanStepTimer.Start();
        }

        private void pacmanStepTimer_Tick(object sender, EventArgs e)
        {
            pacMan.MoveOn();

            panel.Invalidate();
        }
    }
}
