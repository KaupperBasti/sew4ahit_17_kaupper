﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PacMan
{
    public enum Direction
    {
        None = -1,
        North = 0,
        East = 1,
        West = 2,
        South = 3
    }

    class PacMan
    {
        private Color color;
        private int width;
        private Point position;
        private int stepWidth;
        private int _tolerance = 5;

        private Direction direction;
        private Direction _requestedDirection;

        private Size panelSize;

        private double _maxAngle = 45;

        private int _duration = 15;
        private int _tickCounter = 0;

        public PacMan(Color color, int width, Point startPosition, int stepWidth, Direction startAlignment, Size parentSize)
        {
            this.color = color;
            this.width = width;
            this.position = startPosition;
            this.stepWidth = stepWidth;
            this.direction = startAlignment;
            this.panelSize = parentSize;

            this._requestedDirection = Direction.None;
        }

        public void MoveOn()
        {
            position.X += stepWidth * (direction == Direction.East ? 1 : direction == Direction.West ? -1 : 0);

            if (position.X >= panelSize.Width - width / 2 || position.X <= width / 2)
                position.X -= stepWidth * (direction == Direction.East ? 1 : direction == Direction.West ? -1 : 0);

            position.Y += stepWidth * (direction == Direction.South ? 1 : direction == Direction.North ? -1 : 0);
            if (position.Y >= panelSize.Height - width / 2 || position.Y <= width / 2)
                position.Y -= stepWidth * (direction == Direction.South ? 1 : direction == Direction.North ? -1 : 0);

            if (_requestedDirection != Direction.None)
            {
                if ((_requestedDirection == Direction.East || _requestedDirection == Direction.West) && (direction == Direction.East || direction == Direction.West))
                    direction = _requestedDirection;
                else if ((_requestedDirection == Direction.South || _requestedDirection == Direction.North) && (direction == Direction.South || direction == Direction.North))
                    direction = _requestedDirection;
                else if ((position.X % width < _tolerance || width - position.X % width < _tolerance) && (position.Y % width < _tolerance || width - position.Y % width < _tolerance))
                {
                    direction = _requestedDirection;

                    if (position.X % width < _tolerance)
                        position.X -= position.X % width;
                    else
                        position.X += (width - position.X % width);

                    if (position.Y % width < _tolerance)
                        position.Y -= position.Y % width;
                    else
                        position.Y += (width - position.Y % width);
                }

                if(direction == _requestedDirection)
                    _requestedDirection = Direction.None;
            }
        }

        public void DrawMe(Graphics g)
        {
            int currentAngle = (int)(Math.Abs(_maxAngle * Math.Sin((Math.PI) / (double)_duration * _tickCounter)));
            _tickCounter = (_tickCounter + 1) % _duration;

            int additionAngle = (direction == Direction.East ? 0 : (direction == Direction.West ? 180 : (direction == Direction.South ? 90 : 270)));
            g.FillPie(new SolidBrush(color), position.X - width / 2, position.Y - width / 2, width, width, additionAngle + currentAngle, (360 - 2 * currentAngle));
        }

        public void ResizePanel(Size s)
        {
            this.panelSize = s;
        }

        public void RequestDirectionChange(Direction d)
        {
            this._requestedDirection = d;
        }
    }
}
