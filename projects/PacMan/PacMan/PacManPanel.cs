﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PacMan
{
    class PacManPanel : Panel
    {
        private PacMan pacMan = null;

        public void AddPacMan(PacMan p)
        {
            this.pacMan = p;
        }

        new public void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    pacMan.RequestDirectionChange(Direction.North);
                    break;
                case Keys.Down:
                    pacMan.RequestDirectionChange(Direction.South);
                    break;
                case Keys.Left:
                    pacMan.RequestDirectionChange(Direction.West);
                    break;
                case Keys.Right:
                    pacMan.RequestDirectionChange(Direction.East);
                    break;
                default:
                    break;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (pacMan != null)
                pacMan.DrawMe(e.Graphics);
            base.OnPaint(e);
        }

        protected override void OnResize(EventArgs eventargs)
        {
            if (pacMan != null)
                pacMan.ResizePanel(Size);
            base.OnResize(eventargs);
        }
    }
}
