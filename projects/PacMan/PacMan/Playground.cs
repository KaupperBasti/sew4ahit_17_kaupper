﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PacMan
{
    class Playground
    {
        private int width;
        private int height;
        private int distance;

        public Playground(int width, int height, int distance)
        {
            this.width = width;
            this.height = height;
            this.distance = distance;
        }

        public void Draw(Graphics g)
        {
            int offset = distance / 2;
            Pen p = new Pen(Brushes.LightSlateGray);

            // horizontal
            for (int i = 0; offset + distance * i < height; i++)
                g.DrawLine(p, new Point(0, offset + distance * i), new Point(width, offset + distance * i));
            
            // vertikal
            for (int i = 0; offset + distance * i < width; i++)
                g.DrawLine(p, new Point(offset + distance * i, 0), new Point(offset + distance * i, height));

        }
    }
}
