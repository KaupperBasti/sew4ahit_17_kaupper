﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreenSaver
{
    public partial class Form1 : Form
    {
        List<Point> linePoints;
        List<Color> colors;
        Random randy;

        public Form1()
        {
            InitializeComponent();
            this.linePoints = new List<Point>();
            this.colors = new List<Color>();
            linePoints.Add(new Point(0, 0));

            this.Paint += Form1_Paint;
            this.randy = new Random(DateTime.Now.Millisecond);
        }

        void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            linePoints.Add(new Point(randy.Next(this.ClientRectangle.Width), randy.Next(this.ClientRectangle.Height)));

            if (linePoints.Count > 6)
                linePoints.RemoveAt(0);

            colors.Add(Color.FromArgb(randy.Next(256),randy.Next(256),randy.Next(256)));

            if (colors.Count > 5)
                colors.RemoveAt(0);

            for (int i = 0; i < 5 && i < linePoints.Count - 1; i++)
                g.DrawLine(new Pen(colors[i]), linePoints[i], linePoints[i+1]);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }
    }
}
