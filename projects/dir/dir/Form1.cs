﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace dir
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += Form1_Load;    
        }

        void Form1_Load(object sender, EventArgs e)
        {
            //string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string path = "C:\\";
            TreeNode t = treeView1.Nodes.Add(path);
            t.Tag = path;
           // fill(path, t, -1);
        }

        private void treeView1_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            string dir = e.Node.Tag.ToString();
            DirectoryInfo d = new DirectoryInfo(dir);

            string info = "";
            info += "LastAccessTime: " + d.LastAccessTime;
            label1.Text = info;
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                string dir = e.Node.Tag.ToString();
                string[] files = Directory.GetFiles(dir);

                for (int i = 0; i < files.Length; i++)
                    files[i] = Path.GetFileName(files[i]);

                listBox1.Items.Clear();
                listBox1.Items.AddRange(files);

                if (e.Node.Nodes.Count == 0)
                    fill(dir, e.Node, 0);

                e.Node.Expand();
            }
            catch { }
        }

        void fill(string dir, TreeNode parent, int levels)
        {
            try
            {
                string[] dirs = Directory.GetDirectories(dir);

                foreach (string s in dirs)
                {
                    TreeNode t = parent.Nodes.Add(s.Substring(s.LastIndexOf('\\') + 1));
                    t.Tag = s;

                    if(levels == -1 || levels > 0)
                        fill(s, t, levels == -1 ? -1 : levels-1);
                }
            }
            catch{}
        }
    }
}
