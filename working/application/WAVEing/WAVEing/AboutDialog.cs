﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    public partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();

            // Versionsnummer hinzufügen
            label1.Text += " v" + GlobalSettings.VersionID;
        }

        private void AboutDialog_KeyDown(object sender, KeyEventArgs e)
        {
            // Dialog bei einem Tastendruck schließen
            Close();
        }
    }
}
