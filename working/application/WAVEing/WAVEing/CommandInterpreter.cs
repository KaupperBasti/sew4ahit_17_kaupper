﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    class CommandInterpreter
    {
        public static Wave PreviewWave = null;
        public static Wave PreviewDeletedWave = null;

        /// <summary>
        /// Alle zwischengespeicherten Änderungen verwerfen
        /// </summary>
        public static void ClearPreview()
        {
            if (PreviewWave != null)
                Storage.Waves.Remove(PreviewWave);
            if (PreviewDeletedWave != null)
                Storage.Waves.Add(PreviewDeletedWave);

            PreviewDeletedWave = PreviewWave = null;
        }

        /// <summary>
        /// Ein Kommando interpretieren und anschließend ausführen.
        /// </summary>
        /// <param name="command">Das entsprechende Kommando.</param>
        /// <param name="preview">Gibt an, ob das Kommando zur Erzeugung einer Vorschau dient.</param>
        /// <param name="mw">Das Hauptfenster.</param>
        public static void InterpretCommand(string command, bool preview, MainWindow mw)
        {
            // Alle 'null' Werte löschen!
            Storage.Waves.RemoveAll((w) => w == null);

            // Alle Vorschauänderungen werden gelöscht.
            if (!preview)
                ClearPreview();


            string function = "";
            Dictionary<string, string> arguments = new Dictionary<string, string>();

            if (command == "")
                return;

            List<string> buffer = command.Split(' ').ToList();

            function = buffer[0];
            buffer.RemoveAt(0);


            string option = "";
            string value = "";

            // Über die verschiedenen Optionen iterieren.
            for (int i = 0; i < buffer.Count; i++)
            {
                // ParameterID
                if (buffer[i].StartsWith("-"))
                {
                    if (buffer[i].Trim('-') == "")
                        continue;
                    else if (option != "")
                        arguments.Add(option, "");
                    option = buffer[i].Trim('-');
                }
                // Parameterwert
                else
                {
                    if (option == "")
                        continue;
                    
                    value = buffer[i];
                    arguments.Add(option, value);

                    option = "";
                }
            }
            // Kommando ausführen
            ProcessCommand(function, preview, arguments, mw);
        }

        /// <summary>
        /// Führt ein übergebenes Kommando aus  mit Parametern aus!
        /// </summary>
        /// <param name="function">Die aufgerufene Funktion.</param>
        /// <param name="preview">Gibt an, ob das Kommando zur Erzeugung einer Vorschau dient.</param>
        /// <param name="arguments">Die Parameter der Funktion.</param>
        /// <param name="mw">Das Hauptfenster</param>
        private static void ProcessCommand(string function, bool preview, Dictionary<string,string> arguments, MainWindow mw)
        {
            Storage.Waves.RemoveAll((w) => w == null);

            Wave wave = new Wave();
            bool reloadAtException = false;
            try
            {
                // Funktionsswitch
                switch (function.ToUpper())
                {
                    case "LIST":
                        if (preview)
                            break;

                        // Listet alle Wellen mit ihren Eigenschaften auf.
                        for(int i = 0; i < Math.Ceiling((double)Storage.Waves.Count/5); i++)
                        {
                            string s = "";

                            for (int j = 5 * i; j < 5 * i + 5 && j < Storage.Waves.Count; j++)
                                s += Storage.Waves[j].Name.PadRight(15, ' ') + "" + Storage.Waves[j].Offset.ToString().PadLeft(10, ' ') + "ms Off." + Storage.Waves[j].Duration.ToString().PadLeft(10, ' ') + "ms Dur." + Storage.Waves[j].Amplitude.ToString().PadLeft(5, ' ') + "% Amp." + Storage.Waves[j].Frequency.ToString().PadLeft(5, ' ') + "Hz Freq.\n";

                            new MessageDialog(s, "LIST ("+(i+1)+"/"+Math.Ceiling((double)Storage.Waves.Count/5)+")").ShowDialog();
                        }
                        break;

                    case "ADDWAVE":
                        string[] neccessaryArgs = new string[] { "f", "d", "a", "n" };
                        
                        if(PreviewWave!= null)
                        {
                            Storage.Waves.Remove(PreviewWave);
                            reloadAtException = true;
                        }
                        PreviewWave = null;

                        // Prüft, ob alle Parameter vorhanden sind.
                        if (arguments.Keys.Intersect(neccessaryArgs).Count() < neccessaryArgs.Length)
                            break;

                        if (arguments.ContainsKey("o"))
                            wave.Offset = int.Parse(arguments["o"]);

                        wave.Amplitude = int.Parse(arguments["a"]);
                        wave.Duration = int.Parse(arguments["d"]);
                        wave.Frequency = int.Parse(arguments["f"]);
                        wave.Name = arguments["n"];

                       

                        // Validierung der Parameter.
                        wave.Name.ToCharArray().ToList().ForEach((c) =>
                        {
                            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-' || c == '_')
                                return;
                            throw new ArgumentException("Name darf keine Sonderzeichen außer '-' und '_' beinhalten!");
                        }); 
                        
                        if (wave.Name == "")
                            throw new ArgumentException("Es wurde kein Name eingegeben!");


                        if (Storage.Waves.FindAll((w) => w.Name == wave.Name).Count > 0)
                            throw new ArgumentException("Name ist nicht einzigartig!");

                        // Hinzufügen der Welle.
                        Storage.Waves.Add(PreviewWave = wave);

                        // Wenn es keine Vorschau werden soll, muss PreviewWave gelöscht werden!!
                        if (!preview)
                            PreviewWave = null;

                        mw.GetGraph().ClearData();
                        mw.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
                        mw.InvalidateGraph();
                        break;

                    case "CHANGEWAVE":
                        neccessaryArgs = new string[] { "f", "d", "a", "n" };

                        // Prüft, ob alle Parameter vorhanden sind.
                        if (arguments.Keys.Intersect(neccessaryArgs).Count() < neccessaryArgs.Length)
                            break;

                        if (PreviewWave != null)
                        {
                            Storage.Waves.Remove(PreviewWave);
                            reloadAtException = true;
                        }

                        if(PreviewDeletedWave != null)
                        {
                            Storage.Waves.Add(PreviewDeletedWave);
                            reloadAtException = true;
                        }
                        PreviewDeletedWave = PreviewWave = null;

                        Wave buff;
                        // Sucht eine Welle mit dem gegebenen Namen.
                        if ((buff = Storage.Waves.Find((w) => w.Name == arguments["n"])) != null)
                            Storage.Waves.Remove(PreviewDeletedWave = buff);
                        else
                            throw new ArgumentException("Name existiert nicht!");


                        // Wenn es keine Vorschau werden soll, muss PreviewDeletedWave gelöscht werden!!
                        if (!preview)
                            PreviewDeletedWave = null;

                        if (arguments.ContainsKey("o"))
                            wave.Offset = int.Parse(arguments["o"]);

                        wave.Amplitude = int.Parse(arguments["a"]);
                        wave.Duration = int.Parse(arguments["d"]);
                        wave.Frequency = int.Parse(arguments["f"]);
                        wave.Name = arguments["n"];


                        // Validierung der Parameter.
                        if (Storage.Waves.FindAll((w) => w.Name == wave.Name).Count > 0)
                            throw new ArgumentException("Name ist nicht einzigartig!");

                        if (wave.Name == "")
                            throw new ArgumentException("Es wurde kein Name eingegeben!");


                        // Hinzufügen der Welle.
                        Storage.Waves.Add(PreviewWave = wave);

                        // Wenn es keine Vorschau werden soll, muss PreviewWave gelöscht werden!!
                        if (!preview)
                            PreviewWave = null;

                        mw.GetGraph().ClearData();
                        mw.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
                        mw.InvalidateGraph();
                        break;

                    case "DELETEWAVE":
                        neccessaryArgs = new string[] { "n" };

                        // Prüft, ob alle Parameter vorhanden sind.
                        if (arguments.Keys.Intersect(neccessaryArgs).Count() < neccessaryArgs.Length)
                            break;

                        if (PreviewDeletedWave != null)
                            Storage.Waves.Add(PreviewDeletedWave);
                        
                        // Sucht nach der zu löschenden Welle.
                        PreviewDeletedWave = Storage.Waves.Find((w) => w.Name == arguments["n"]);
                        if(PreviewDeletedWave != null)
                            Storage.Waves.Remove(PreviewDeletedWave);

                        // Wenn es keine Vorschau werden soll, muss PreviewDeletedWave gelöscht werden!!
                        if (!preview)
                            PreviewDeletedWave = null;

                        mw.GetGraph().ClearData();
                        mw.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
                        mw.InvalidateGraph();
                        break;

                    case "RESETPREVIEW":
                    default:
                        // Setzt alle Vorschauen zurück.
                        ClearPreview();
                        
                        mw.GetGraph().ClearData();
                        mw.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
                        mw.InvalidateGraph();
                        break;
                }
            }
            catch (Exception e)
            {
                // In bestimmten Fehlerfällen ist es 
                if (PreviewDeletedWave != null || PreviewWave != null || reloadAtException)
                {
                    ClearPreview();
                    mw.GetGraph().ClearData();
                    mw.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
                    mw.InvalidateGraph();
                }
                throw e;
            }
        }



        public static void ActivatePreview(MainWindow mw)
        {
            if (PreviewWave != null)
                mw.GetGraph().RemoveData(PreviewWave);


            PreviewWave = new Wave();
        }

        public static void DeactivatePreview(MainWindow mw)
        {
            if (PreviewWave != null)
                mw.GetGraph().RemoveData(PreviewWave);

            PreviewWave = null;
        }
    }
}
