﻿namespace WAVEing
{
    partial class ControlWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.availableWaves = new System.Windows.Forms.ComboBox();
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.deleteWaveButton = new System.Windows.Forms.Button();
            this.newWaveButton = new System.Windows.Forms.Button();
            this.saveWaveButton = new System.Windows.Forms.Button();
            this.propertiesPanel = new System.Windows.Forms.Panel();
            this.offsetPanel = new System.Windows.Forms.Panel();
            this.offsetLabel = new System.Windows.Forms.Label();
            this.offsetNumber = new System.Windows.Forms.NumericUpDown();
            this.durationPanel = new System.Windows.Forms.Panel();
            this.durationNumber = new System.Windows.Forms.NumericUpDown();
            this.durationLabel = new System.Windows.Forms.Label();
            this.separator22 = new System.Windows.Forms.Panel();
            this.separator11 = new System.Windows.Forms.Panel();
            this.propertiesPanel2 = new System.Windows.Forms.Panel();
            this.amplitudePanel = new System.Windows.Forms.Panel();
            this.amplitudeLabel = new System.Windows.Forms.Label();
            this.amplitudeNumber = new System.Windows.Forms.NumericUpDown();
            this.frequencyPanel = new System.Windows.Forms.Panel();
            this.frequencyNumber = new System.Windows.Forms.NumericUpDown();
            this.frequencyLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.separator3 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.waveNamePanel = new System.Windows.Forms.Panel();
            this.waveNameTextBox = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.waveNameLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.previewCheckBox = new System.Windows.Forms.CheckBox();
            this.buttonPanel.SuspendLayout();
            this.propertiesPanel.SuspendLayout();
            this.offsetPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetNumber)).BeginInit();
            this.durationPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.durationNumber)).BeginInit();
            this.propertiesPanel2.SuspendLayout();
            this.amplitudePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.amplitudeNumber)).BeginInit();
            this.frequencyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.waveNamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // availableWaves
            // 
            this.availableWaves.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.availableWaves.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.availableWaves.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.availableWaves.FormattingEnabled = true;
            this.availableWaves.Location = new System.Drawing.Point(0, 0);
            this.availableWaves.Name = "availableWaves";
            this.availableWaves.Size = new System.Drawing.Size(342, 21);
            this.availableWaves.TabIndex = 0;
            this.availableWaves.SelectedIndexChanged += new System.EventHandler(this.availableWaves_SelectedIndexChanged);
            // 
            // buttonPanel
            // 
            this.buttonPanel.Controls.Add(this.deleteWaveButton);
            this.buttonPanel.Controls.Add(this.newWaveButton);
            this.buttonPanel.Controls.Add(this.saveWaveButton);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonPanel.Location = new System.Drawing.Point(0, 196);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(341, 32);
            this.buttonPanel.TabIndex = 100;
            // 
            // deleteWaveButton
            // 
            this.deleteWaveButton.BackColor = System.Drawing.Color.White;
            this.deleteWaveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteWaveButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.deleteWaveButton.FlatAppearance.BorderSize = 0;
            this.deleteWaveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.deleteWaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.deleteWaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteWaveButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.deleteWaveButton.Location = new System.Drawing.Point(100, 0);
            this.deleteWaveButton.Name = "deleteWaveButton";
            this.deleteWaveButton.Size = new System.Drawing.Size(141, 32);
            this.deleteWaveButton.TabIndex = 1;
            this.deleteWaveButton.Text = "Delete";
            this.deleteWaveButton.UseVisualStyleBackColor = false;
            this.deleteWaveButton.Click += new System.EventHandler(this.deleteWaveButton_Click);
            // 
            // newWaveButton
            // 
            this.newWaveButton.BackColor = System.Drawing.Color.White;
            this.newWaveButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.newWaveButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.newWaveButton.FlatAppearance.BorderSize = 0;
            this.newWaveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.newWaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.newWaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newWaveButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.newWaveButton.Location = new System.Drawing.Point(0, 0);
            this.newWaveButton.Name = "newWaveButton";
            this.newWaveButton.Size = new System.Drawing.Size(100, 32);
            this.newWaveButton.TabIndex = 0;
            this.newWaveButton.Text = "New";
            this.newWaveButton.UseVisualStyleBackColor = false;
            this.newWaveButton.Click += new System.EventHandler(this.newWaveButton_Click);
            // 
            // saveWaveButton
            // 
            this.saveWaveButton.BackColor = System.Drawing.Color.White;
            this.saveWaveButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.saveWaveButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.saveWaveButton.FlatAppearance.BorderSize = 0;
            this.saveWaveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.saveWaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.saveWaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveWaveButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.saveWaveButton.Location = new System.Drawing.Point(241, 0);
            this.saveWaveButton.Name = "saveWaveButton";
            this.saveWaveButton.Size = new System.Drawing.Size(100, 32);
            this.saveWaveButton.TabIndex = 2;
            this.saveWaveButton.Text = "Save";
            this.saveWaveButton.UseVisualStyleBackColor = false;
            this.saveWaveButton.Click += new System.EventHandler(this.saveWaveButton_Click);
            // 
            // propertiesPanel
            // 
            this.propertiesPanel.Controls.Add(this.offsetPanel);
            this.propertiesPanel.Controls.Add(this.durationPanel);
            this.propertiesPanel.Controls.Add(this.separator22);
            this.propertiesPanel.Controls.Add(this.separator11);
            this.propertiesPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.propertiesPanel.Name = "propertiesPanel";
            this.propertiesPanel.Size = new System.Drawing.Size(342, 38);
            this.propertiesPanel.TabIndex = 1;
            // 
            // offsetPanel
            // 
            this.offsetPanel.Controls.Add(this.offsetLabel);
            this.offsetPanel.Controls.Add(this.offsetNumber);
            this.offsetPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.offsetPanel.Location = new System.Drawing.Point(5, 0);
            this.offsetPanel.Name = "offsetPanel";
            this.offsetPanel.Size = new System.Drawing.Size(116, 38);
            this.offsetPanel.TabIndex = 2;
            // 
            // offsetLabel
            // 
            this.offsetLabel.AutoSize = true;
            this.offsetLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.offsetLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.offsetLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.offsetLabel.Location = new System.Drawing.Point(0, 0);
            this.offsetLabel.Name = "offsetLabel";
            this.offsetLabel.Size = new System.Drawing.Size(84, 16);
            this.offsetLabel.TabIndex = 100;
            this.offsetLabel.Text = "Offset (ms)";
            // 
            // offsetNumber
            // 
            this.offsetNumber.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.offsetNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.offsetNumber.Location = new System.Drawing.Point(0, 18);
            this.offsetNumber.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.offsetNumber.Name = "offsetNumber";
            this.offsetNumber.Size = new System.Drawing.Size(116, 20);
            this.offsetNumber.TabIndex = 0;
            this.offsetNumber.ThousandsSeparator = true;
            this.offsetNumber.ValueChanged += new System.EventHandler(this.valueChanged);
            // 
            // durationPanel
            // 
            this.durationPanel.Controls.Add(this.durationNumber);
            this.durationPanel.Controls.Add(this.durationLabel);
            this.durationPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.durationPanel.Location = new System.Drawing.Point(224, 0);
            this.durationPanel.Name = "durationPanel";
            this.durationPanel.Size = new System.Drawing.Size(113, 38);
            this.durationPanel.TabIndex = 3;
            // 
            // durationNumber
            // 
            this.durationNumber.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.durationNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.durationNumber.Location = new System.Drawing.Point(0, 18);
            this.durationNumber.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.durationNumber.Name = "durationNumber";
            this.durationNumber.Size = new System.Drawing.Size(113, 20);
            this.durationNumber.TabIndex = 0;
            this.durationNumber.ThousandsSeparator = true;
            this.durationNumber.ValueChanged += new System.EventHandler(this.valueChanged);
            // 
            // durationLabel
            // 
            this.durationLabel.AutoSize = true;
            this.durationLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.durationLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.durationLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.durationLabel.Location = new System.Drawing.Point(0, 0);
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(98, 16);
            this.durationLabel.TabIndex = 100;
            this.durationLabel.Text = "Duration (ms)";
            this.durationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // separator22
            // 
            this.separator22.BackColor = System.Drawing.SystemColors.Control;
            this.separator22.Dock = System.Windows.Forms.DockStyle.Right;
            this.separator22.Location = new System.Drawing.Point(337, 0);
            this.separator22.Name = "separator22";
            this.separator22.Size = new System.Drawing.Size(5, 38);
            this.separator22.TabIndex = 100;
            // 
            // separator11
            // 
            this.separator11.BackColor = System.Drawing.SystemColors.Control;
            this.separator11.Dock = System.Windows.Forms.DockStyle.Left;
            this.separator11.Location = new System.Drawing.Point(0, 0);
            this.separator11.Name = "separator11";
            this.separator11.Size = new System.Drawing.Size(5, 38);
            this.separator11.TabIndex = 100;
            // 
            // propertiesPanel2
            // 
            this.propertiesPanel2.Controls.Add(this.amplitudePanel);
            this.propertiesPanel2.Controls.Add(this.frequencyPanel);
            this.propertiesPanel2.Controls.Add(this.panel1);
            this.propertiesPanel2.Controls.Add(this.separator3);
            this.propertiesPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertiesPanel2.Location = new System.Drawing.Point(0, 38);
            this.propertiesPanel2.Name = "propertiesPanel2";
            this.propertiesPanel2.Size = new System.Drawing.Size(342, 38);
            this.propertiesPanel2.TabIndex = 2;
            // 
            // amplitudePanel
            // 
            this.amplitudePanel.Controls.Add(this.amplitudeLabel);
            this.amplitudePanel.Controls.Add(this.amplitudeNumber);
            this.amplitudePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.amplitudePanel.Location = new System.Drawing.Point(5, 0);
            this.amplitudePanel.Name = "amplitudePanel";
            this.amplitudePanel.Size = new System.Drawing.Size(116, 38);
            this.amplitudePanel.TabIndex = 2;
            // 
            // amplitudeLabel
            // 
            this.amplitudeLabel.AutoSize = true;
            this.amplitudeLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.amplitudeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.amplitudeLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.amplitudeLabel.Location = new System.Drawing.Point(0, 0);
            this.amplitudeLabel.Name = "amplitudeLabel";
            this.amplitudeLabel.Size = new System.Drawing.Size(102, 16);
            this.amplitudeLabel.TabIndex = 100;
            this.amplitudeLabel.Text = "Amplitude (%)";
            // 
            // amplitudeNumber
            // 
            this.amplitudeNumber.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.amplitudeNumber.Location = new System.Drawing.Point(0, 18);
            this.amplitudeNumber.Name = "amplitudeNumber";
            this.amplitudeNumber.Size = new System.Drawing.Size(116, 20);
            this.amplitudeNumber.TabIndex = 0;
            this.amplitudeNumber.ThousandsSeparator = true;
            this.amplitudeNumber.ValueChanged += new System.EventHandler(this.valueChanged);
            // 
            // frequencyPanel
            // 
            this.frequencyPanel.BackColor = System.Drawing.SystemColors.Control;
            this.frequencyPanel.Controls.Add(this.frequencyNumber);
            this.frequencyPanel.Controls.Add(this.frequencyLabel);
            this.frequencyPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.frequencyPanel.Location = new System.Drawing.Point(224, 0);
            this.frequencyPanel.Name = "frequencyPanel";
            this.frequencyPanel.Size = new System.Drawing.Size(113, 38);
            this.frequencyPanel.TabIndex = 3;
            // 
            // frequencyNumber
            // 
            this.frequencyNumber.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.frequencyNumber.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.frequencyNumber.Location = new System.Drawing.Point(0, 18);
            this.frequencyNumber.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.frequencyNumber.Name = "frequencyNumber";
            this.frequencyNumber.Size = new System.Drawing.Size(113, 20);
            this.frequencyNumber.TabIndex = 1;
            this.frequencyNumber.ThousandsSeparator = true;
            this.frequencyNumber.ValueChanged += new System.EventHandler(this.valueChanged);
            // 
            // frequencyLabel
            // 
            this.frequencyLabel.AutoSize = true;
            this.frequencyLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.frequencyLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.frequencyLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.frequencyLabel.Location = new System.Drawing.Point(0, 0);
            this.frequencyLabel.Name = "frequencyLabel";
            this.frequencyLabel.Size = new System.Drawing.Size(110, 16);
            this.frequencyLabel.TabIndex = 100;
            this.frequencyLabel.Text = "Frequency (Hz)";
            this.frequencyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(337, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(5, 38);
            this.panel1.TabIndex = 101;
            // 
            // separator3
            // 
            this.separator3.BackColor = System.Drawing.SystemColors.Control;
            this.separator3.Dock = System.Windows.Forms.DockStyle.Left;
            this.separator3.Location = new System.Drawing.Point(0, 0);
            this.separator3.Name = "separator3";
            this.separator3.Size = new System.Drawing.Size(5, 38);
            this.separator3.TabIndex = 100;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel1.Controls.Add(this.waveNamePanel);
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.availableWaves);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer1.Panel2.Controls.Add(this.panel5);
            this.splitContainer1.Panel2.Controls.Add(this.propertiesPanel2);
            this.splitContainer1.Panel2.Controls.Add(this.propertiesPanel);
            this.splitContainer1.Size = new System.Drawing.Size(342, 168);
            this.splitContainer1.SplitterDistance = 53;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 0;
            // 
            // waveNamePanel
            // 
            this.waveNamePanel.Controls.Add(this.waveNameTextBox);
            this.waveNamePanel.Controls.Add(this.panel3);
            this.waveNamePanel.Controls.Add(this.panel2);
            this.waveNamePanel.Controls.Add(this.waveNameLabel);
            this.waveNamePanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.waveNamePanel.Location = new System.Drawing.Point(0, 25);
            this.waveNamePanel.Name = "waveNamePanel";
            this.waveNamePanel.Size = new System.Drawing.Size(342, 23);
            this.waveNamePanel.TabIndex = 102;
            // 
            // waveNameTextBox
            // 
            this.waveNameTextBox.Dock = System.Windows.Forms.DockStyle.Right;
            this.waveNameTextBox.Location = new System.Drawing.Point(109, 0);
            this.waveNameTextBox.Name = "waveNameTextBox";
            this.waveNameTextBox.Size = new System.Drawing.Size(230, 20);
            this.waveNameTextBox.TabIndex = 103;
            this.waveNameTextBox.TextChanged += new System.EventHandler(this.valueChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(339, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 23);
            this.panel3.TabIndex = 102;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(3, 23);
            this.panel2.TabIndex = 101;
            // 
            // waveNameLabel
            // 
            this.waveNameLabel.AutoSize = true;
            this.waveNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.waveNameLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.waveNameLabel.Location = new System.Drawing.Point(3, 3);
            this.waveNameLabel.Name = "waveNameLabel";
            this.waveNameLabel.Size = new System.Drawing.Size(84, 16);
            this.waveNameLabel.TabIndex = 0;
            this.waveNameLabel.Text = "Wave name";
            this.waveNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 48);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(342, 5);
            this.panel4.TabIndex = 101;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 76);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(342, 5);
            this.panel5.TabIndex = 102;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel2.Controls.Add(this.previewCheckBox);
            this.splitContainer2.Size = new System.Drawing.Size(341, 196);
            this.splitContainer2.SplitterDistance = 164;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 0;
            // 
            // previewCheckBox
            // 
            this.previewCheckBox.AutoSize = true;
            this.previewCheckBox.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.previewCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.previewCheckBox.Location = new System.Drawing.Point(126, 7);
            this.previewCheckBox.Name = "previewCheckBox";
            this.previewCheckBox.Size = new System.Drawing.Size(91, 17);
            this.previewCheckBox.TabIndex = 0;
            this.previewCheckBox.Text = "Show Preview";
            this.previewCheckBox.UseVisualStyleBackColor = true;
            this.previewCheckBox.CheckedChanged += new System.EventHandler(this.previewCheckBox_CheckedChanged);
            // 
            // ControlWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 228);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.buttonPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ControlWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ControlWindow - WAVEing";
            this.Activated += new System.EventHandler(this.ControlWindow_Activated);
            this.Deactivate += new System.EventHandler(this.ControlWindow_Deactivate);
            this.buttonPanel.ResumeLayout(false);
            this.propertiesPanel.ResumeLayout(false);
            this.offsetPanel.ResumeLayout(false);
            this.offsetPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.offsetNumber)).EndInit();
            this.durationPanel.ResumeLayout(false);
            this.durationPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.durationNumber)).EndInit();
            this.propertiesPanel2.ResumeLayout(false);
            this.amplitudePanel.ResumeLayout(false);
            this.amplitudePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.amplitudeNumber)).EndInit();
            this.frequencyPanel.ResumeLayout(false);
            this.frequencyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frequencyNumber)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.waveNamePanel.ResumeLayout(false);
            this.waveNamePanel.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox availableWaves;
        private System.Windows.Forms.Panel buttonPanel;
        private System.Windows.Forms.Button deleteWaveButton;
        private System.Windows.Forms.Button newWaveButton;
        private System.Windows.Forms.Button saveWaveButton;
        private System.Windows.Forms.Panel propertiesPanel;
        private System.Windows.Forms.Panel separator11;
        private System.Windows.Forms.Panel durationPanel;
        private System.Windows.Forms.NumericUpDown durationNumber;
        private System.Windows.Forms.Label durationLabel;
        private System.Windows.Forms.Panel separator22;
        private System.Windows.Forms.Panel propertiesPanel2;
        private System.Windows.Forms.Panel separator3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.CheckBox previewCheckBox;
        private System.Windows.Forms.Panel offsetPanel;
        private System.Windows.Forms.Label offsetLabel;
        private System.Windows.Forms.NumericUpDown offsetNumber;
        private System.Windows.Forms.Panel amplitudePanel;
        private System.Windows.Forms.Label amplitudeLabel;
        private System.Windows.Forms.NumericUpDown amplitudeNumber;
        private System.Windows.Forms.Panel frequencyPanel;
        private System.Windows.Forms.NumericUpDown frequencyNumber;
        private System.Windows.Forms.Label frequencyLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel waveNamePanel;
        private System.Windows.Forms.TextBox waveNameTextBox;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label waveNameLabel;
        private System.Windows.Forms.Panel panel5;
    }
}