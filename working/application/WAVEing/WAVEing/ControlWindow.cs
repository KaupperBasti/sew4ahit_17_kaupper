﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    public partial class ControlWindow : Form
    {
        // Ermöglicht es von außerhalb die Vorschau Checkbox zu setzen
        public bool ShowPreview { get { return previewCheckBox.Checked; } set { previewCheckBox.Checked = value; } }

        private Wave wave;
        private MainWindow mainWindow { get { return Owner as MainWindow; } }

        public ControlWindow()
        {
            InitializeComponent();
            wave = new Wave();
        }

        
        #region ButtonEvents

        private void newWaveButton_Click(object sender, EventArgs e)
        {
            // Alle Boxen werden auf die Standardwerte gesetzt.
            // eventuelle Änderungen verfallen.
            ResetInputs();
            // Resetten von ComboBox und Graph!
            ReloadWaves();
        }

        private void deleteWaveButton_Click(object sender, EventArgs e)
        {
            // Wenn eine alte Welle ausgewählt wurde, wird diese gelöscht.
            if (availableWaves.SelectedIndex >= 0)
                Storage.Waves.Remove(((Wave)availableWaves.SelectedItem));
            else
                Storage.Waves.RemoveAll((w) => w.Name == waveNameTextBox.Text);

            // Projekt wurde verändert!!
            mainWindow.SetUnsaved();

            // Alle Boxen werden auf die Standardwerte gesetzt.
            // eventuelle Änderungen verfallen.
            ResetInputs();
            // Resetten von ComboBox und Graph!
            ReloadWaves();
        }

        private void saveWaveButton_Click(object sender, EventArgs e)
        {
            // Prüft, ob ein Name eingegeben wurde.
            if (waveNameTextBox.Text == "")
            {
                MessageBox.Show("Die Welle muss einen Namen haben!");
                return;
            }
            CommandInterpreter.ClearPreview();
            try
            {
                // Wenn eine Welle ausgewählt wurde, wird diese entfernt.
                if (availableWaves.SelectedIndex == -1 && Storage.Waves.Find((w) => w.Name == wave.Name) == null)
                    CommandInterpreter.InterpretCommand("ADDWAVE -o " + wave.Offset + " -d " + wave.Duration + " -f " + wave.Frequency + " -a " + wave.Amplitude + " -n " + wave.Name, false, mainWindow);
                else
                    CommandInterpreter.InterpretCommand("CHANGEWAVE -o " + wave.Offset + " -d " + wave.Duration + " -f " + wave.Frequency + " -a " + wave.Amplitude + " -n " + wave.Name, false, mainWindow);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Während der Verarbeitung der Daten ist ein Fehler aufgetreten!!\n(" + ex.Message + ")", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FormatException ex)
            {
                MessageBox.Show("The input data has the wrong type!\nThis error shouldn't be possible!!\n("+ex.Message+")", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            wave = new Wave();
            // Projekt wurde verändert!!
            mainWindow.SetUnsaved();


            // Alle Boxen werden auf die Standardwerte gesetzt.
            ResetInputs();
            // Resetten von ComboBox und Graph!
            ReloadWaves();
        }

        #endregion

        #region OtherControlEvents

        private void availableWaves_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Wenn ein Element ausgewählt wurde.
            if ((sender as ComboBox).SelectedIndex >= 0)
            {
                // Werte der ausgewählten Welle setzen.
                Wave temp = (sender as ComboBox).SelectedItem as Wave;
                waveNameTextBox.Text = temp.Name;
                frequencyNumber.Value = (decimal)temp.Frequency;
                durationNumber.Value = (decimal)temp.Duration;
                amplitudeNumber.Value = (decimal)temp.Amplitude;
                offsetNumber.Value = (decimal)temp.Offset;

                wave = new Wave(temp.Name, temp.Offset, temp.Duration, temp.Frequency, temp.Amplitude);
                temp = null;

                try
                {
                    if (previewCheckBox.Checked)
                        CommandInterpreter.InterpretCommand("CHANGEWAVE -o " + wave.Offset + " -d " + wave.Duration + " -f " + wave.Frequency + " -a " + wave.Amplitude + " -n " + wave.Name, true, mainWindow);
                }
                catch 
                { 
                    // Vorschau Fehler werden ignoriert!
                }
            }
        }

        private void valueChanged(object sender, EventArgs e)
        {
            // Prüft, welches Control das Event ausgelöst hat.
            // Die Vorschauwelle wird dementsprechend angepasst!
            switch ((sender as Control).Name)
            {
                case "waveNameTextBox":
                    wave.Name = (sender as TextBox).Text;
                    break;

                // Das Parsen muss nicht überwacht werden, da es sich um das Control NumericUpDown handelt!
                case "amplitudeNumber":
                    wave.Amplitude = decimal.ToDouble((sender as NumericUpDown).Value);
                    break;
                case "frequencyNumber":
                    wave.Frequency = decimal.ToDouble((sender as NumericUpDown).Value);
                    break;
                case "offsetNumber":
                    wave.Offset = decimal.ToDouble((sender as NumericUpDown).Value);
                    break;
                case "durationNumber":
                    wave.Duration = decimal.ToDouble((sender as NumericUpDown).Value);
                    break;
            }

            try
            {
                if (previewCheckBox.Checked)
                {
                    CommandInterpreter.ClearPreview();

                    if (availableWaves.SelectedIndex == -1 && Storage.Waves.Find((w) => w.Name == wave.Name) == null)
                        CommandInterpreter.InterpretCommand("ADDWAVE -o " + wave.Offset.ToString() + " -d " + wave.Duration.ToString() + " -f " + wave.Frequency.ToString() + " -a " + wave.Amplitude.ToString() + " -n " + wave.Name.ToString(), true, mainWindow);
                    else if (Storage.Waves.Count > 0)
                        CommandInterpreter.InterpretCommand("CHANGEWAVE -o " + wave.Offset.ToString() + " -d " + wave.Duration.ToString() + " -f " + wave.Frequency.ToString() + " -a " + wave.Amplitude.ToString() + " -n " + wave.Name.ToString(), true, mainWindow);
                }
                else
                    CommandInterpreter.InterpretCommand("RESETPREVIEW", true, mainWindow);
            }
            catch 
            {
                // Vorschau Fehler werden ignoriert!
            }

        }

        private void previewCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            GlobalSettings.EnableControlWindowPreview = (sender as CheckBox).Checked;

            try
            {
                // Vorschau anzeigen, wenn die Option gesetzt ist.
                if (GlobalSettings.EnableControlWindowPreview)
                {
                    if (availableWaves.SelectedIndex == -1 && Storage.Waves.Find((w) => w.Name == wave.Name) == null)
                        CommandInterpreter.InterpretCommand("ADDWAVE -o " + wave.Offset.ToString() + " -d " + wave.Duration.ToString() + " -f " + wave.Frequency.ToString() + " -a " + wave.Amplitude.ToString() + " -n " + wave.Name.ToString(), true, mainWindow);
                    else
                        CommandInterpreter.InterpretCommand("CHANGEWAVE -o " + wave.Offset.ToString() + " -d " + wave.Duration.ToString() + " -f " + wave.Frequency.ToString() + " -a " + wave.Amplitude.ToString() + " -n " + wave.Name.ToString(), true, mainWindow);
                }
                else
                {
                    CommandInterpreter.InterpretCommand("RESETPREVIEW", true, mainWindow);
                }
            }
            catch
            {
                // Vorschaufehler werden ignoriert!
            }
        }

        #endregion

        
        #region HelpFunctions

        /// <summary>
        /// Resetted die ComboBox und den Graphen!
        /// </summary>
        public void ReloadWaves()
        {
            // Die ComboBox wird neu befüllt.
            availableWaves.Items.Clear();
            availableWaves.Items.AddRange(Storage.Waves.ToArray<object>());

            // Der Graph wird neu befüllt und neu gezeichnet.
            mainWindow.GetGraph().ClearData();
            mainWindow.GetGraph().AddDataRange(Storage.Waves.ToList<ITimeValues>());
            mainWindow.InvalidateGraph();
        }

        private void ResetInputs()
        {
            // Alle Standardwerte wiederherstellen.
            previewCheckBox.Checked = false;
            availableWaves.SelectedIndex = -1;
            waveNameTextBox.Text = "";
            amplitudeNumber.Value = 0;
            frequencyNumber.Value = 0;
            offsetNumber.Value = 0;
            durationNumber.Value = 0;
        }

        #endregion

        private void ControlWindow_Activated(object sender, EventArgs e)
        {
            ReloadWaves();
        }

        private void ControlWindow_Deactivate(object sender, EventArgs e)
        {
           // previewCheckBox.Checked = false;
        }
    }
}
