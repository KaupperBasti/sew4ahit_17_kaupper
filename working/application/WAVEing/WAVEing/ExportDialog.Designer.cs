﻿namespace WAVEing
{
    partial class ExportDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBarExport = new System.Windows.Forms.ProgressBar();
            this.canelExportButton = new System.Windows.Forms.Button();
            this.labelExport = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressBarExport
            // 
            this.progressBarExport.Location = new System.Drawing.Point(12, 34);
            this.progressBarExport.Name = "progressBarExport";
            this.progressBarExport.Size = new System.Drawing.Size(309, 23);
            this.progressBarExport.TabIndex = 0;
            // 
            // canelExportButton
            // 
            this.canelExportButton.BackColor = System.Drawing.Color.White;
            this.canelExportButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.canelExportButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.canelExportButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.canelExportButton.FlatAppearance.BorderSize = 0;
            this.canelExportButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.canelExportButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.canelExportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.canelExportButton.Font = GlobalSettings.DefaultFont;
            this.canelExportButton.Location = new System.Drawing.Point(0, 65);
            this.canelExportButton.Name = "canelExportButton";
            this.canelExportButton.Size = new System.Drawing.Size(329, 33);
            this.canelExportButton.TabIndex = 8;
            this.canelExportButton.Text = "Cancel";
            this.canelExportButton.UseVisualStyleBackColor = false;
            this.canelExportButton.Click += new System.EventHandler(this.canelExportButton_Click);
            // 
            // labelExport
            // 
            this.labelExport.AutoSize = true;
            this.labelExport.Location = new System.Drawing.Point(9, 9);
            this.labelExport.Name = "labelExport";
            this.labelExport.Size = new System.Drawing.Size(35, 13);
            this.labelExport.TabIndex = 9;
            this.labelExport.Text = "label1";
            // 
            // ExportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 98);
            this.ControlBox = false;
            this.Controls.Add(this.labelExport);
            this.Controls.Add(this.canelExportButton);
            this.Controls.Add(this.progressBarExport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ExportDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Export as WAV";
            this.Load += new System.EventHandler(this.ExportDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBarExport;
        private System.Windows.Forms.Button canelExportButton;
        private System.Windows.Forms.Label labelExport;
    }
}