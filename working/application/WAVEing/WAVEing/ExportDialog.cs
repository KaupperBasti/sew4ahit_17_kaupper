﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace WAVEing
{
    public partial class ExportDialog : Form
    {
        private string fileName;
        new private delegate void Invoke();

        private Thread progressThread;
        private Thread saveThread;

        private FileStream fs;

        public ExportDialog(string fileName)
        {
            InitializeComponent();

            this.fileName = fileName;
            labelExport.Text = "Export '" + Path.GetFileName(GlobalSettings.CurrentProject.FileName) + "' as '" + Path.GetFileName(fileName) + "'...";
            this.FormClosing += new FormClosingEventHandler(ExportDialog_FormClosing);
        }

        void ExportDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Alle laufenden Threads abbrechen!
            if(saveThread.IsAlive)
                saveThread.Abort();
            if(progressThread.IsAlive)
                progressThread.Abort();

            // Wernn der Stream noch offen ist, wird er geschlossen.
            if (fs != null && fs.CanRead)
                fs.Close();
        }

        private void ExportDialog_Load(object sender, EventArgs e)
        {
            // Liste erstellen mit den zu exportierenden Wellen.
            List<ITimeValues> exportList = Storage.Waves.ToList<ITimeValues>();
            if (Storage.WavFile != null)
                exportList.Add(Storage.WavFile);

            // Die Datei öffnen!
            fs = new FileStream(fileName, FileMode.Create);

            (progressThread = new Thread(() =>
            {
                // Der Fortschritt des Exports wird regelmäßig neu gelesen.
                while (progressBarExport.Value <= 100)
                {
                    if (this.IsDisposed)
                        return;

                    this.Invoke(new Invoke(() =>
                    {
                        progressBarExport.Value = (int)WaveFileGenerator.Percentage;
                    }));
                    // CPU Zeit sparen
                    Thread.Sleep(10);
                }
            })).Start();

            (saveThread = new Thread(() =>
            {
                // Export starten.
                GlobalSettings.Exporting = true;
                if (WaveFileGenerator.SaveToFile(fs, exportList, ((MainWindow)Owner).GetGraph().Length, GlobalSettings.CurrentProject))
                {
                    // Noch eine kurze Zeit warten, damit die Statusanzeige fertig wird.
                    Thread.Sleep(100);
                    MessageBox.Show("Export war erfolgreich!", "Erfolgreich!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Exportieren war nicht erfolgreich!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                GlobalSettings.Exporting = false;

                // Exportfenster schließen
                this.Invoke(new Invoke(() => Close()));
            })).Start();
        }

        private void canelExportButton_Click(object sender, EventArgs e)
        {
            // Alle laufenden Threads abbrechen
            if (saveThread.IsAlive)
                saveThread.Abort();
            if (progressThread.IsAlive)
                progressThread.Abort();

            // Stream schließen
            if (fs != null && fs.CanRead)
                fs.Close();

            // Halbfertige Datei löschen
            if (File.Exists(fileName))
                File.Delete(fileName);
        }
    }
}
