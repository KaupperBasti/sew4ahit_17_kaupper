﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WAVEing
{
    /// <summary>
    /// Eine Sammlung an unterstützten Audioformaten (ausbaufähig!!)
    /// </summary>
    public enum Formats
    {
        Wave = 0,
        NotSupported = 1
    }
}
