﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace WAVEing
{
    public enum Channels
    {
        Mono = 1,
        Stereo = 2
    }

    public enum BPS
    {
        Acht = 8,
        Sechzehn = 16,
        ZweiUndDreißig = 32
    }

    class GlobalSettings
    {
        // Aktuelles Projekt!
        public static ProjectSettings CurrentProject = null;

        public static Channels DefaultChannels = Channels.Mono;
        public static BPS DefaultBitsPerSample = BPS.Acht;
        public static int DefaultSampleRate = 44100;
        public static Formats DefaultFormat = Formats.Wave;
        public static string DefaultProjectFileName = "waving_project.wvprj";
        public static string DefaultProjectFileDirectory = ".";

        public static bool ShowControlWindow = true;
        public static bool EnableControlWindowPreview = true;
        public static bool ShowCommandLine = true;
        public static bool EnableCommandLinePreview = true;
        public static bool EnableTabCompletion = true;


        public static string VersionID = "2.0";
        public static bool Exporting = false;
        public static Font DefaultFont = new Font("Verdana", 9.75f);


        // Dateien sind begrenzt auf 100000ms = 1000s = 16min40s
        public static long MaximumFileLengthMs = 1000000;


        /// <summary>
        /// Lädt die globalen Einstellungen aus der angegebenen Datei.
        /// </summary>
        /// <param name="fileName">Relativer Dateipfad der Konfigurationsdatei.</param>
        /// <returns>Konnte die Datei erfolgreich gelesen werden?</returns>
        public static bool LoadFromFile(string fileName)
        {
            FileStream fs = null;
            // Wenn die Datei nicht existiert, wird false zurückgegeben.
            if (!File.Exists(fileName))
                return false;

            string currentElementName = "";
            XmlReader re = null;
            try
            {
                // XmlReader wird initialisiert
                // Ohne den FileStream, kann die Datei anschließend nicht ordnungsgemäß geschlossen werden!
                re = XmlReader.Create(fs = new FileStream(fileName, FileMode.Open));

                // Iteriert durch alle Knoten der XML-Datei
                // Aufbau der Datei: siehe it.pdf!
                while (re.Read())
                {
                    switch (re.NodeType)
                    {
                        //Falls der Knoten ein XML-Tag ist
                        case XmlNodeType.Element:
                            switch (re.Name)
                            {
                                // Der nächste Knoten enthält die Versionsnummer.
                                case "version":
                                    currentElementName = "version";
                                    break;
                                // Wenn ein Wert erwartet wird, muss das Namensattribut zwischengespeichert werden.
                                case "value":
                                    currentElementName = re.GetAttribute("name");
                                    break;
                            }
                            break;

                        // Falls der Knoten eine Textzeile ist, werden die Werte dem entsprechenden Optionen zugeteilt.
                        // Das (oberflächliche) Exceptionhandling übernimmt das try-catch.
                        case XmlNodeType.Text:
                            switch (currentElementName)
                            {
                                case "controlwindow":
                                    ShowControlWindow = bool.Parse(re.Value);
                                    break;

                                case "controlwindowpreview":
                                    EnableControlWindowPreview = bool.Parse(re.Value);
                                    break;

                                case "commandline":
                                    ShowCommandLine = bool.Parse(re.Value);
                                    break;

                                case "commandlinepreview":
                                    EnableCommandLinePreview = bool.Parse(re.Value);
                                    break;

                                case "tabcompletion":
                                    EnableTabCompletion = bool.Parse(re.Value);
                                    break;

                                case "samplerate":
                                    DefaultSampleRate = int.Parse(re.Value);
                                    break;

                                case "bitspersample":
                                    DefaultBitsPerSample = (BPS)int.Parse(re.Value);
                                    break;

                                case "channels":
                                    DefaultChannels = (Channels)int.Parse(re.Value);
                                    break;

                                case "format":
                                    DefaultFormat = (Formats)int.Parse(re.Value);
                                    break;

                                case "filename":
                                    DefaultProjectFileName = Path.ChangeExtension(re.Value, ".wvprj");
                                    break;

                                case "savedirectory":
                                    DefaultProjectFileDirectory = re.Value;
                                    break;
                            }
                            break;

                        // Bei einem Endtag, wird der zwischengespeicherte Wert gelöscht.
                        case XmlNodeType.EndElement:
                            currentElementName = "";
                            break;
                    }
                }
            }
            catch
            {
                // Bei einem Parsefehler, wird eine (oberflächliche) Nachricht an den User gesendet.
                MessageBox.Show("Invalid config file!");
                re.Close();
                fs.Close();
                return false;
            }
            re.Close();
            fs.Close();
            return true;
        }

        /// <summary>
        /// Speichert die globalen Einstellung in die angegebenen Datei.
        /// </summary>
        /// <param name="fileName">Relativer Dateipfad der Konfigurationsdatei.</param>
        /// <returns>Konnte die Datei erfolgreich gespeichert werden?</returns>
        public static bool SaveToFile(string fileName)
        {
            // Prüft, ob der Ordner existiert.
            if (!Directory.Exists(Path.GetDirectoryName(Path.GetFullPath(fileName))))
            {
                MessageBox.Show("Failed to save config!\nDirectory not found!");
                return false;
            }

            XmlWriter wr;
            try
            {
                // Öffnet den XmlWriter.
                wr = XmlWriter.Create(fileName);
            }
            catch
            {
                MessageBox.Show("Failed to save config!\nFile error!");
                return false;
            }


            // Schreiben der XML-Datei.
            // Aufbau der Datei: siehe it.pdf

            wr.WriteStartDocument();

            wr.WriteRaw("\r\n");
            wr.WriteStartElement("config");

            wr.WriteRaw("\r\n\t");
            wr.WriteComment("Versionsnummer des Programms");

            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("version");
            wr.WriteString(VersionID);
            wr.WriteEndElement();


            wr.WriteRaw("\r\n\r\n\t");
            wr.WriteComment("Fenstereinstellungen");
            wr.WriteRaw("\r\n\t");
            wr.WriteComment("(View- bzw. Settings-Tab; siehe screen.pdf)");

            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("settings");

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "controlwindow");
            wr.WriteString(ShowControlWindow.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "controlwindowpreview");
            wr.WriteString(EnableControlWindowPreview.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "commandline");
            wr.WriteString(ShowCommandLine.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "commandlinepreview");
            wr.WriteString(EnableCommandLinePreview.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "tabcompletion");
            wr.WriteString(EnableTabCompletion.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t");
            wr.WriteEndElement();


            wr.WriteRaw("\r\n\r\n\t");
            wr.WriteComment("Standardeinstellungen für neue Projekte");

            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("defaults");

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "samplerate");
            wr.WriteString(DefaultSampleRate.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "bitspersample");
            wr.WriteString(((int)DefaultBitsPerSample).ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "channels");
            wr.WriteString(((int)DefaultChannels).ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "format");
            wr.WriteString(((int)DefaultFormat).ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "filename");
            wr.WriteString(DefaultProjectFileName.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "savedirectory");
            wr.WriteString(DefaultProjectFileDirectory.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t");
            wr.WriteEndElement();

            wr.WriteRaw("\r\n");
            wr.Close();

            return true;
        }
    }
}
