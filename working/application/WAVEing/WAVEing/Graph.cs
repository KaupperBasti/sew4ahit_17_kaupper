﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace WAVEing
{
    public class Graph
    {
        //Panel, in dem die Graph-Klasse arbeiten darf.
        private Panel parentPanel;

        //Panel, in dem der Graph erstellt wird.
        private Panel graphPanel;
        //Scrollbar zum Scrollen des Graphen
        private HScrollBar scrollBar;

        //Dimensionen der Achsenpfeile ( ◄- )
        int arrowSize = 5;
        int arrowWidth = 12;
        //Arrays mit den Punkten der Achsenpfeile
        List<Point[]> basicGraphPoints;

        //sämtliche Daten Interfaces (siehe Wave-Klasse!)
        private List<ITimeValues> data;


        //Variable die eine Änderung der Daten anzeigt (Cache ungültig!)
        private bool hasDataChanged = true;
        //Gecachte Länge der aktuellen Gesamtschwingung
        private long _length = 0;
        //Berechnung der Gesamtlänge der Schwingung
        private long length
        {
            get
            {
                if (!hasDataChanged)
                    return _length;
                hasDataChanged = false;

                if (data == null)
                    return 0;

                long max = 0;
                foreach (ITimeValues o in data)
                {
                    bool started = false;
                    int time = 0;
                    while (time < GlobalSettings.MaximumFileLengthMs)
                    {
                        double value = 0.0;
                        if (!started && (value = o.GetValueAt(time)) != 0)
                            started = true;

                        if (started && value == 0 && o.GetValueAt(time+1) == 0)
                            break;
                        time++;
                    }

                    if (max < time - 1)
                        max = time - 1;
                }

                return _length = max;
            }
        }
        public long Length { get { return _length; } }


        //Graphics Objekt der Zeichenfläche
        private Graphics graphicContext;
        //Skalierungsfaktor für das Zeichnen der Amplituden
        private double verticalScaleFactor;
        //Skalierungsfaktor für das Zeichnen der Schwingungen (horizontal!)
        private double horizontalScaleFactor;
        private const int HORIZONTAL_DIFFERENCE = 100;


        //ersichtliches Intervall der Schwingungen in Millisekunden!
        private Tuple<double, double> timeAxis;

        //maximal angezeigte Amplitude in Prozent
        private double maximumAmplitude;

        //Projektoptionen
        private ProjectSettings settings;


        //Setzen jeder Eigenschaft auf einen gültigen Wert (darf nicht von außen aufgerufen werden!)
        public Graph()
        {
            parentPanel = null;
            timeAxis = new Tuple<double, double>(0, 0);
            basicGraphPoints = new List<Point[]>();
            settings = null;

            horizontalScaleFactor = 10;
            verticalScaleFactor = 1;
            graphicContext = null;
            data = new List<ITimeValues>();

            maximumAmplitude = 100.0;
        }

        public Graph(ProjectSettings settings)
            : this()
        {
            this.settings = settings;
        }

        public Graph(Graphics graphic, ProjectSettings settings)
            : this(settings)
        {
            graphicContext = graphic;
        }

        public Graph(Graphics graphic, List<ITimeValues> data, ProjectSettings settings)
            : this(graphic, settings)
        {
            this.data = data;

            CalculateVerticalScaleFactor();
        }



        /// <summary>
        /// Setzt das Elternelement der Zeichenfläche/Scrollbar
        /// </summary>
        /// <param name="p">Das Elternelement, in das gezeichnet werden soll.</param>
        public void SetParentPanel(Panel p)
        {
            //zweitmaliges Setzen nicht möglich!
            if (parentPanel != null)
                return;

            parentPanel = p;
            //Panel leeren damit keine unbekannten Controls enthalten sind!
            p.Controls.Clear();
            p.Controls.Add(graphPanel = new Panel());
            p.Controls.Add(scrollBar = new HScrollBar());

            //Farbe des Graphen setzen
            graphPanel.BackColor = Color.White;
            //Das ganze Elterncontrol ausfüllen.
            graphPanel.Dock = DockStyle.Fill;
            //Paintevent festlegen
            graphPanel.Paint += (sender, e) => Draw(e.Graphics);
            graphPanel.SizeChanged += (sender, e) => CalculateAxisArrows();


            //Scrollbar unter dem Graphen andocken.
            scrollBar.Dock = DockStyle.Bottom;
            //Wertänderung bei Drücken der Navigationspfeile
            scrollBar.SmallChange = 100;
            //Wertänderung bei Ziehen der Scrollleiste
            scrollBar.LargeChange = 150;
            //Scrollbar Grenzen berechnen!
            RecalculateScrollbar();

            //Beim Betätigen der Scrollpaar werden die Zeitkoordinaten neu berechnet und der Graph neu gezeichnet.
            scrollBar.ValueChanged += (sender, e) =>
            {
                timeAxis = new Tuple<double, double>(scrollBar.Value, Math.Ceiling(scrollBar.Value + HORIZONTAL_DIFFERENCE * horizontalScaleFactor));
                graphPanel.Invalidate();
            };

            //Zeitkoordinaten auf den Ausgangswert setzen!
            timeAxis = new Tuple<double, double>(0, p.Width);

        }

        private void CalculateAxisArrows()
        {
            //Koordinatenpunkte der Achsenpfeile vorberechnen.
            basicGraphPoints = new List<Point[]>();
            basicGraphPoints.Add(new Point[] { new Point(0, graphPanel.Height / 2), new Point(arrowWidth, graphPanel.Height / 2 + arrowSize), new Point(arrowWidth, graphPanel.Height / 2 - arrowSize) });
            basicGraphPoints.Add(new Point[] { new Point(graphPanel.Width, graphPanel.Height / 2), new Point(graphPanel.Width - arrowWidth, graphPanel.Height / 2 + arrowSize), new Point(graphPanel.Width - arrowWidth, graphPanel.Height / 2 - arrowSize) });
            basicGraphPoints.Add(new Point[] { new Point(graphPanel.Width / 2, 0), new Point(graphPanel.Width / 2 - arrowSize, arrowWidth), new Point(graphPanel.Width / 2 + arrowSize, arrowWidth) });
            basicGraphPoints.Add(new Point[] { new Point(graphPanel.Width / 2, graphPanel.Height), new Point(graphPanel.Width / 2 - arrowSize, graphPanel.Height - arrowWidth), new Point(graphPanel.Width / 2 + arrowSize, graphPanel.Height - arrowWidth) });
        }

        /// <summary>
        /// Ändert den horizontalen Zoom.
        /// </summary>
        /// <param name="changeZoomPercentage">Die Faktoränderung des Zooms.</param>
        public void AddZoom(double changeZoom)
        {
            if (parentPanel == null || settings == null)
                return;

            if(horizontalScaleFactor + changeZoom > 0)
                horizontalScaleFactor += changeZoom;

            //Scrollbar an den neuen Zoomfaktor anpassen!
            RecalculateScrollbar();

            //Graph neu zeichnen
            graphPanel.Invalidate();
        }

        /// <summary>
        /// Fügt ein neues Datenobjekt in die Liste ein und passt den vertikalen Zoom an.
        /// </summary>
        /// <param name="data">Das hinzuzufügende Datenobjekt</param>
        public void AddData(ITimeValues data)
        {
            if (parentPanel == null || settings == null)
                return;

            this.data.Add(data);
            hasDataChanged = true;
            CalculateVerticalScaleFactor();
            RecalculateScrollbar();
            graphPanel.Invalidate();
        
        }

        /// <summary>
        /// Fügt eine Lister neuer Datenobjekte in die Liste ein und passt den vertikalen Zoom an.
        /// </summary>
        /// <param name="data">Die hinzuzufügenden Datenobjekte</param>
        public void AddDataRange(List<ITimeValues> data)
        {
            if (parentPanel == null || settings == null)
                return;

            this.data.AddRange(data);
            hasDataChanged = true;
            RecalculateScrollbar();
            CalculateVerticalScaleFactor();
            graphPanel.Invalidate();
        }

        /// <summary>
        /// Löscht alle bereits hinzugefügten Datenobjekte.
        /// </summary>
        public void ClearData()
        {
            if (parentPanel == null || settings == null)
                return;

            this.data.Clear();
            if(Storage.WavFile != null)
                this.data.Add(Storage.WavFile);
            hasDataChanged = true;
            RecalculateScrollbar();
            graphPanel.Invalidate();
        }

        /// <summary>
        /// Löscht ein Datenobjekt aus der Liste und passt den vertikalen Zoom an
        /// </summary>
        /// <param name="data">Das zu löschende Datenobjekt</param>
        public void RemoveData(ITimeValues data)
        {
            if (parentPanel == null || settings == null)
              return;
            
            this.data.Remove(data);
            hasDataChanged = true;
            CalculateVerticalScaleFactor();
            RecalculateScrollbar();
            graphPanel.Invalidate();
        }

        /// <summary>
        /// Legt neue Optionen für das Zeichnen des Graphens fest.
        /// </summary>
        /// <param name="settings">Die neuen Projekteinstellungen.</param>
        public void SetProjectSettings(ProjectSettings settings)
        {
            this.settings = settings;
            hasDataChanged = true;
            RecalculateScrollbar();
            CalculateVerticalScaleFactor();
            graphPanel.Invalidate();
        }


        /// <summary>
        /// Zeichnet den Graphen in das durch SetParentPanel festgelegte Elternelement!
        /// </summary>
        /// <param name="g">Das Graphicsobjekt der Zeichenfläche</param>
        private void Draw(Graphics g)
        {
            //Graphics Objekt übergeben und zeichnen
            graphicContext = g;
            Draw();
        }

        /// <summary>
        /// Zeichnet den Graphen in das durch SetParentPanel festgelegte Elternelement mit dem zuvor übergebenen Graphicsobjekt!
        /// </summary>
        public void Draw()
        {
            if (graphicContext == null)
                return;
            graphicContext.SmoothingMode = SmoothingMode.HighQuality;

            //Anzahl der berechneten Punkte pro Zeichenfläche
            int n = 200;
            //Anzahl der Pixel pro Sample
            double pixelPerSample = (double)graphPanel.Width / (double)(n - 1);
            //Anzahl der angezeigten Millisekunden
            double timeWidth = timeAxis.Item2 - timeAxis.Item1;

            
            Pen axisPen = new Pen(Brushes.Black, 1);

            //X-Achse
            graphicContext.DrawLine(axisPen, new Point(0, graphPanel.Height / 2), new Point(graphPanel.Width, graphPanel.Height / 2));
            //Y-Achse
            graphicContext.DrawLine(axisPen, new Point(graphPanel.Width / 2, 0), new Point(graphPanel.Width / 2, graphPanel.Height));


            Pen valuePen = new Pen(Brushes.Red, 2.5f);

            //Zeichnen der skalierten Sinuswelle
            double last = 0;
            double t = 0;
            for (int i = 0; i < n; i++, t += timeWidth / n)
            {
                double current = 0;
                foreach (ITimeValues v in data)
                    current -= v.GetValueAt(t + scrollBar.Value);

                if (i != 0)
                {
                    float x1 = (float)pixelPerSample * i;
                    float y1 = (float)((float)(graphPanel.Height / 2) / maximumAmplitude * last * verticalScaleFactor) + graphPanel.Height / 2;

                    float x2 = (float)pixelPerSample * (i + 1);
                    float y2 = (float)((float)(graphPanel.Height / 2) / maximumAmplitude * current * verticalScaleFactor) + graphPanel.Height / 2;


                    //Wenn der Überlaufschutz ausgeschalten ist, werden zu große Amplituden abgeschnitten!
                    if (y1 < 0)
                        y1 = 0;
                    else if (y1 > graphPanel.Height)
                        y1 = graphPanel.Height - 1;

                    if (y2 < 0)
                        y2 = 0;
                    else if (y2 > graphPanel.Height)
                        y2 = graphPanel.Height - 1;

                    graphicContext.DrawLine(valuePen, new PointF(x1, y1), new PointF(x2, y2));
                }

                last = current;
            }

            valuePen.Dispose();



            //obere Achsenbeschriftung Y
            graphicContext.DrawString(Math.Pow(2, (settings != null ? (int)settings.BitsPerSample - 1 : 8)).ToString(), GlobalSettings.DefaultFont, Brushes.Black, graphPanel.Width / 2 + 2 * arrowSize, 0, StringFormat.GenericDefault);
            //untere Achsenbeschriftung Y
            graphicContext.DrawString("-" + Math.Pow(2, (settings != null ? (int)settings.BitsPerSample - 1 : 8)).ToString(), GlobalSettings.DefaultFont, Brushes.Black, graphPanel.Width / 2 + 2 * arrowSize, graphPanel.Height - GlobalSettings.DefaultFont.Height, StringFormat.GenericDefault);

            //linke Achsenbeschriftung X
            graphicContext.DrawString(timeAxis.Item1.ToString(), GlobalSettings.DefaultFont, Brushes.Black, 0, graphPanel.Height / 2 + 2 * arrowSize, StringFormat.GenericDefault);
            //rechte Achsenbeschriftung X
            graphicContext.DrawString(timeAxis.Item2.ToString(), GlobalSettings.DefaultFont, Brushes.Black, graphPanel.Width - graphicContext.MeasureString(timeAxis.Item2.ToString(), GlobalSettings.DefaultFont).Width, graphPanel.Height / 2 + 2 * arrowSize, StringFormat.GenericDefault);


            //Zeichnen der vorgespeicherten Achsenpfeile
            foreach (Point[] ps in basicGraphPoints)
                graphicContext.FillPolygon(axisPen.Brush, ps);

            axisPen.Dispose();
        }




        /// <summary>
        /// Berechnet die Werte der Scrollbar anhand des aktuellen Zooms neu.
        /// </summary>
        private void RecalculateScrollbar()
        {
            if (parentPanel == null || settings == null)
                return;

            //Zahl der angezeigten Millisekunden am Bildschirm!
            double msPerScreen = horizontalScaleFactor * HORIZONTAL_DIFFERENCE;


            //Maximalwert der Scrollbar berechnen
            //Dieser wird genau auf die Länge der Sinuswellen angepasst!
            int max = (int)Math.Ceiling(length - msPerScreen);
            //Bei zu kleinem Wert wird das Maximum gleich dem Minimum gesetzt!
            scrollBar.Maximum = max < scrollBar.Minimum ? scrollBar.Minimum : max;
            //Anpassen des Maximumwertes um 1 Wertänderung (notwendig!)
            scrollBar.Maximum += scrollBar.LargeChange;


            //Zeitkoordinaten anpassen
            timeAxis = new Tuple<double, double>(scrollBar.Value, Math.Ceiling(scrollBar.Value + msPerScreen));
        }

        /// <summary>
        /// Berechnet den vertikalen Zoom anhand der aktuellen Daten.
        /// </summary>
        private void CalculateVerticalScaleFactor()
        {
            if (parentPanel == null || settings == null)
                return;

            if (data == null)
                return;

            //Wenn der Überflussschutz ausgeschalten ist, wird kein vertikaler Faktor berechnet!
            if (settings != null && settings.CutOverflow)
            {
                verticalScaleFactor = 1;
                return;
            }

            //Amplituden für die gesamte Länge berechnen
            double amp = 0;
            double max = 0;
            for (int i = 0; i < length; i++)
                foreach (ITimeValues v in data)
                {
                    amp -= v.GetValueAt(i);
                    if (amp > max)
                        max = amp;
                }

            //berechnen des Skalierungsfaktors!
            verticalScaleFactor = Math.Min(maximumAmplitude / max, 1);
        }
    }
}
