﻿namespace WAVEing
{
    partial class HelpDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.howButton = new System.Windows.Forms.Button();
            this.whatButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // howButton
            // 
            this.howButton.BackColor = System.Drawing.Color.White;
            this.howButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.howButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.howButton.FlatAppearance.BorderSize = 0;
            this.howButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.howButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.howButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.howButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.howButton.Location = new System.Drawing.Point(0, 32);
            this.howButton.Name = "howButton";
            this.howButton.Size = new System.Drawing.Size(307, 33);
            this.howButton.TabIndex = 11;
            this.howButton.Text = "How it works";
            this.howButton.UseVisualStyleBackColor = false;
            this.howButton.Click += new System.EventHandler(this.howButton_Click);
            // 
            // whatButton
            // 
            this.whatButton.BackColor = System.Drawing.Color.White;
            this.whatButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.whatButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.whatButton.FlatAppearance.BorderSize = 0;
            this.whatButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.whatButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.whatButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.whatButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.whatButton.Location = new System.Drawing.Point(0, -1);
            this.whatButton.Name = "whatButton";
            this.whatButton.Size = new System.Drawing.Size(307, 33);
            this.whatButton.TabIndex = 13;
            this.whatButton.Text = "What is it?";
            this.whatButton.UseVisualStyleBackColor = false;
            this.whatButton.Click += new System.EventHandler(this.whatButton_Click);
            // 
            // HelpDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 65);
            this.Controls.Add(this.whatButton);
            this.Controls.Add(this.howButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HelpDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Help";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button howButton;
        private System.Windows.Forms.Button whatButton;
    }
}