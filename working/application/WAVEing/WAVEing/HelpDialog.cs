﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    /// <summary>
    /// Dient zur Anzeige der Hilfenachrichten (selbsterklärend!)
    /// </summary>
    public partial class HelpDialog : Form
    {
        public HelpDialog()
        {
            InitializeComponent();
        }

        private void whatButton_Click(object sender, EventArgs e)
        {
            string message = "WAVEing is a school project for creating audio files out of simple sinus waves.\n";
            message+="It is not meant to create whole songs but small sounds.\n\n";
            message += "Import and export from or to external files surely is provided but importing wav files slows down all processes like exporting due to the complexity of extracting waves out of audio files (see also 'Heisenberg uncertainty principle').\n";

            new MessageDialog(message, "What is it?").ShowDialog();
        }

        private void howButton_Click(object sender, EventArgs e)
        {
            string message = "";
            
            message += "To create waves there are 2 possibilities:\n";
            message += "- The control window\n";
            message += "- The commandline\n\n";

            message += "The control window is quite self describing.\n";
            message += "You have to enter all values required (all textboxes!).\n";
            message += "If you have already added a wave, you can choose it from the dropdown box and change its values\n\n";
            
            message += "The commandline is not that hard to use, though.\n";
            message += "There are 4 commands available now:\n";
            message += "- ADDWAVE\n";
            message += "- CHANGEWAVE\n";
            message += "- DELETEWAVE\n";
            message += "- LIST\n\n";

            message += "The first two have 5 parameters.\n";
            message += "- -n for the name of the wave\n";
            message += "- -o for the offset of the wave\n";
            message += "- -d for the duration\n";
            message += "- -a for the amplitude\n";
            message += "- -f for the frequency\n";
            message += "The CHANGEWAVE command has to be given the name of a already existing wave.\n";
            message += "DELETEWAVE takes the name of a wave and delete it.\n";
            message += "LIST shows all added waves in a messagebox.\n\n";

            message += "If you enabled tab completion, you are able to write 'a' and with the tab key you can jump from a parameter to another.\n\n";

            message += "The only setting worth mentioning is 'cut overflow'.\n";
            message += "Disabling this option let the graph rescale all waves if the amplitude increases over 100 percent.\n";
            message += "This is useful if you have a lot of waves and want to export is as an audio file.\n";

            new MessageDialog(message, "How it works.").Show();
        }
    }
}
