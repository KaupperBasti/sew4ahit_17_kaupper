﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WAVEing
{
    public interface ITimeValues
    {
        double GetValueAt(double ms);
    }
}
