﻿namespace WAVEing
{
    partial class MainWindow
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveProjectAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importWAVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportWAVMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.showControlWindowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showCommandlineMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.commandlineTabCompletionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commandlinePreviewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.projectsettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moreSettingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.commandLineExecuteButton = new System.Windows.Forms.Button();
            this.commandLineTextBox = new System.Windows.Forms.TextBox();
            this.commandLinePanel = new System.Windows.Forms.Panel();
            this.graphPanel = new WAVEing.WAVEPanel();
            this.exportWaves = new System.ComponentModel.BackgroundWorker();
            this.mainMenu.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.commandLinePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStrip,
            this.viewToolStrip,
            this.settingsToolStrip});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(764, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileToolStrip
            // 
            this.fileToolStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMenuItem,
            this.openMenuItem,
            this.saveProjectMenuItem,
            this.toolStripSeparator1,
            this.saveProjectAsMenuItem,
            this.importWAVToolStripMenuItem,
            this.exportWAVMenuItem,
            this.toolStripSeparator2,
            this.exitMenuItem});
            this.fileToolStrip.Name = "fileToolStrip";
            this.fileToolStrip.ShortcutKeyDisplayString = "";
            this.fileToolStrip.Size = new System.Drawing.Size(37, 20);
            this.fileToolStrip.Text = "File";
            // 
            // newMenuItem
            // 
            this.newMenuItem.Name = "newMenuItem";
            this.newMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newMenuItem.Size = new System.Drawing.Size(286, 22);
            this.newMenuItem.Text = "New...";
            this.newMenuItem.Click += new System.EventHandler(this.newMenuItem_Click);
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openMenuItem.Size = new System.Drawing.Size(286, 22);
            this.openMenuItem.Text = "Open...";
            this.openMenuItem.Click += new System.EventHandler(this.openMenuItem_Click);
            // 
            // saveProjectMenuItem
            // 
            this.saveProjectMenuItem.Enabled = false;
            this.saveProjectMenuItem.Name = "saveProjectMenuItem";
            this.saveProjectMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveProjectMenuItem.Size = new System.Drawing.Size(286, 22);
            this.saveProjectMenuItem.Text = "Save Project";
            this.saveProjectMenuItem.Click += new System.EventHandler(this.saveProjectMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(283, 6);
            // 
            // saveProjectAsMenuItem
            // 
            this.saveProjectAsMenuItem.Enabled = false;
            this.saveProjectAsMenuItem.Name = "saveProjectAsMenuItem";
            this.saveProjectAsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveProjectAsMenuItem.Size = new System.Drawing.Size(286, 22);
            this.saveProjectAsMenuItem.Text = "Save Project as...";
            this.saveProjectAsMenuItem.Click += new System.EventHandler(this.saveProjectAsMenuItem_Click);
            // 
            // importWAVToolStripMenuItem
            // 
            this.importWAVToolStripMenuItem.Enabled = false;
            this.importWAVToolStripMenuItem.Name = "importWAVToolStripMenuItem";
            this.importWAVToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.importWAVToolStripMenuItem.Size = new System.Drawing.Size(286, 22);
            this.importWAVToolStripMenuItem.Text = "Import WAV...";
            this.importWAVToolStripMenuItem.Click += new System.EventHandler(this.importWAVToolStripMenuItem_Click);
            // 
            // exportWAVMenuItem
            // 
            this.exportWAVMenuItem.Enabled = false;
            this.exportWAVMenuItem.Name = "exportWAVMenuItem";
            this.exportWAVMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exportWAVMenuItem.Size = new System.Drawing.Size(286, 22);
            this.exportWAVMenuItem.Text = "Export WAV...";
            this.exportWAVMenuItem.Click += new System.EventHandler(this.exportWAVMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(283, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitMenuItem.Size = new System.Drawing.Size(286, 22);
            this.exitMenuItem.Text = "Exit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // viewToolStrip
            // 
            this.viewToolStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showControlWindowMenuItem,
            this.showCommandlineMenuItem});
            this.viewToolStrip.Name = "viewToolStrip";
            this.viewToolStrip.Size = new System.Drawing.Size(44, 20);
            this.viewToolStrip.Text = "View";
            // 
            // showControlWindowMenuItem
            // 
            this.showControlWindowMenuItem.Checked = true;
            this.showControlWindowMenuItem.CheckOnClick = true;
            this.showControlWindowMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showControlWindowMenuItem.Name = "showControlWindowMenuItem";
            this.showControlWindowMenuItem.Size = new System.Drawing.Size(193, 22);
            this.showControlWindowMenuItem.Text = "Show Control Window";
            this.showControlWindowMenuItem.Click += new System.EventHandler(this.showControlWindowMenuItem_Click);
            // 
            // showCommandlineMenuItem
            // 
            this.showCommandlineMenuItem.Checked = true;
            this.showCommandlineMenuItem.CheckOnClick = true;
            this.showCommandlineMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showCommandlineMenuItem.Name = "showCommandlineMenuItem";
            this.showCommandlineMenuItem.Size = new System.Drawing.Size(193, 22);
            this.showCommandlineMenuItem.Text = "Show Commandline";
            this.showCommandlineMenuItem.Click += new System.EventHandler(this.showCommandlineMenuItem_Click);
            // 
            // settingsToolStrip
            // 
            this.settingsToolStrip.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commandlineTabCompletionMenuItem,
            this.commandlinePreviewMenuItem,
            this.toolStripSeparator3,
            this.projectsettingsToolStripMenuItem,
            this.moreSettingsMenuItem,
            this.toolStripSeparator4,
            this.helpMenuItem,
            this.aboutMenuItem});
            this.settingsToolStrip.Name = "settingsToolStrip";
            this.settingsToolStrip.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStrip.Text = "Settings";
            // 
            // commandlineTabCompletionMenuItem
            // 
            this.commandlineTabCompletionMenuItem.Checked = true;
            this.commandlineTabCompletionMenuItem.CheckOnClick = true;
            this.commandlineTabCompletionMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.commandlineTabCompletionMenuItem.Name = "commandlineTabCompletionMenuItem";
            this.commandlineTabCompletionMenuItem.Size = new System.Drawing.Size(241, 22);
            this.commandlineTabCompletionMenuItem.Text = "Commandline Tab-Completion";
            this.commandlineTabCompletionMenuItem.CheckStateChanged += new System.EventHandler(this.commandlineTabCompletionMenuItem_CheckStateChanged);
            // 
            // commandlinePreviewMenuItem
            // 
            this.commandlinePreviewMenuItem.Checked = true;
            this.commandlinePreviewMenuItem.CheckOnClick = true;
            this.commandlinePreviewMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.commandlinePreviewMenuItem.Name = "commandlinePreviewMenuItem";
            this.commandlinePreviewMenuItem.Size = new System.Drawing.Size(241, 22);
            this.commandlinePreviewMenuItem.Text = "Commandline Preview";
            this.commandlinePreviewMenuItem.CheckStateChanged += new System.EventHandler(this.commandlinePreviewMenuItem_CheckStateChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(238, 6);
            // 
            // projectsettingsToolStripMenuItem
            // 
            this.projectsettingsToolStripMenuItem.Enabled = false;
            this.projectsettingsToolStripMenuItem.Name = "projectsettingsToolStripMenuItem";
            this.projectsettingsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.projectsettingsToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.projectsettingsToolStripMenuItem.Text = "Project Settings...";
            this.projectsettingsToolStripMenuItem.Click += new System.EventHandler(this.projectsettingsToolStripMenuItem_Click);
            // 
            // moreSettingsMenuItem
            // 
            this.moreSettingsMenuItem.Name = "moreSettingsMenuItem";
            this.moreSettingsMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.moreSettingsMenuItem.Size = new System.Drawing.Size(241, 22);
            this.moreSettingsMenuItem.Text = "More Settings...";
            this.moreSettingsMenuItem.Click += new System.EventHandler(this.moreSettingsMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(238, 6);
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpMenuItem.Size = new System.Drawing.Size(241, 22);
            this.helpMenuItem.Text = "Help...";
            this.helpMenuItem.Click += new System.EventHandler(this.helpMenuItem_Click);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.aboutMenuItem.Size = new System.Drawing.Size(241, 22);
            this.aboutMenuItem.Text = "About...";
            this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 404);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(764, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(98, 17);
            this.statusLabel.Text = "Unknown project";
            // 
            // commandLineExecuteButton
            // 
            this.commandLineExecuteButton.BackColor = System.Drawing.Color.White;
            this.commandLineExecuteButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.commandLineExecuteButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.commandLineExecuteButton.FlatAppearance.BorderSize = 0;
            this.commandLineExecuteButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.commandLineExecuteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.commandLineExecuteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.commandLineExecuteButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.commandLineExecuteButton.Location = new System.Drawing.Point(689, 0);
            this.commandLineExecuteButton.Name = "commandLineExecuteButton";
            this.commandLineExecuteButton.Size = new System.Drawing.Size(75, 30);
            this.commandLineExecuteButton.TabIndex = 4;
            this.commandLineExecuteButton.Text = "Execute";
            this.commandLineExecuteButton.UseVisualStyleBackColor = false;
            this.commandLineExecuteButton.Click += new System.EventHandler(this.commandLineExecuteButton_Click);
            // 
            // commandLineTextBox
            // 
            this.commandLineTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.commandLineTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commandLineTextBox.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.commandLineTextBox.Location = new System.Drawing.Point(0, 0);
            this.commandLineTextBox.Name = "commandLineTextBox";
            this.commandLineTextBox.Size = new System.Drawing.Size(689, 16);
            this.commandLineTextBox.TabIndex = 3;
            this.commandLineTextBox.Click += new System.EventHandler(this.commandLineTextBox_Click);
            this.commandLineTextBox.TextChanged += new System.EventHandler(this.commandLineTextBox_TextChanged);
            this.commandLineTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLineTextBox_KeyDown);
            this.commandLineTextBox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.commandLineTextBox_PreviewKeyDown);
            // 
            // commandLinePanel
            // 
            this.commandLinePanel.BackColor = System.Drawing.SystemColors.Window;
            this.commandLinePanel.Controls.Add(this.commandLineTextBox);
            this.commandLinePanel.Controls.Add(this.commandLineExecuteButton);
            this.commandLinePanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.commandLinePanel.Location = new System.Drawing.Point(0, 374);
            this.commandLinePanel.Name = "commandLinePanel";
            this.commandLinePanel.Size = new System.Drawing.Size(764, 30);
            this.commandLinePanel.TabIndex = 4;
            this.commandLinePanel.Visible = false;
            // 
            // graphPanel
            // 
            this.graphPanel.BackColor = System.Drawing.SystemColors.Control;
            this.graphPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.graphPanel.Location = new System.Drawing.Point(0, 24);
            this.graphPanel.Name = "graphPanel";
            this.graphPanel.Size = new System.Drawing.Size(764, 350);
            this.graphPanel.TabIndex = 5;
            // 
            // exportWaves
            // 
            this.exportWaves.WorkerReportsProgress = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(764, 426);
            this.Controls.Add(this.graphPanel);
            this.Controls.Add(this.commandLinePanel);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.mainMenu;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.Text = "WAVEing";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.commandLinePanel.ResumeLayout(false);
            this.commandLinePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStrip;
        private System.Windows.Forms.ToolStripMenuItem viewToolStrip;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Button commandLineExecuteButton;
        private System.Windows.Forms.TextBox commandLineTextBox;
        private System.Windows.Forms.Panel commandLinePanel;
        private WAVEing.WAVEPanel graphPanel;
        private System.Windows.Forms.ToolStripMenuItem newMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveProjectMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveProjectAsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportWAVMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showControlWindowMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showCommandlineMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandlineTabCompletionMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandlinePreviewMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem moreSettingsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectsettingsToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker exportWaves;
        private System.Windows.Forms.ToolStripMenuItem importWAVToolStripMenuItem;
    }
}

