﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace WAVEing
{
    public partial class MainWindow : Form
    {
        ControlWindow controlWindow;
        // Erlaubt dem Kontrollfenster auf den Graphen zuzugreifen.
        internal Graph graph;



        public MainWindow(string openProject)
        {
            InitializeComponent();

            // FormClosing Event registrieren!
            FormClosing += new FormClosingEventHandler(MainWindow_FormClosing);
            // Konfigurationsdatei laden/erstellen
            if (!GlobalSettings.LoadFromFile("config.xml"))
                MessageBox.Show("Laden der Konfigurationsdatei fehlgeschlagen!\nZurücksetzen auf Standardwerte.", "Konfigurationsfehler");

            // Kommandozeilenparameter verarbeiten.
            if (openProject != null)
            {
                GlobalSettings.CurrentProject = ProjectSettings.Load(openProject);
                if (GlobalSettings.CurrentProject == null)
                    MessageBox.Show("Laden der Projektdatei fehlgeschlagen!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Optionen setzen!
            commandlinePreviewMenuItem.Checked = GlobalSettings.EnableCommandLinePreview;
            commandlineTabCompletionMenuItem.Checked = GlobalSettings.EnableTabCompletion;
            showCommandlineMenuItem.Checked = GlobalSettings.ShowCommandLine;
            showControlWindowMenuItem.Checked = GlobalSettings.ShowControlWindow;
        }


        #region FormEvents

        void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Prüft ob ein Projekt vorhanden ist
            if (GlobalSettings.CurrentProject != null)
            {
                // Wenn bereits eine Datei mit den aktuellen Pfadangaben existiert.
                if (File.Exists(Path.GetFullPath(GlobalSettings.CurrentProject.FileName)))
                {
                    // Der Stern im Statustext symbolisiert ungespeicherte Änderungen.
                    if (statusLabel.Text.StartsWith("*"))
                        if (MessageBox.Show("Ungespeicherte Änderungen speichern?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            // Speichern erfolgreich?
                            if (!GlobalSettings.CurrentProject.Save())
                                // Trotz Fehler beenden?
                                if (MessageBox.Show("Soll trotzdem beendet werden?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.No)
                                    // Beenden abbrechen!
                                    e.Cancel = true;
                }
                else
                {
                    // Projekt wurde noch nicht gespeichert.
                    if (MessageBox.Show("Wollen Sie ihr aktuelles Projekt vor dem Beenden speichern?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        //Projekt speichern
                        saveProjectAsMenuItem_Click(sender, null);

                    // Speichern nicht erfolgreich. Der User entscheidet ob beendet werden soll.
                    if (!File.Exists(Path.GetFullPath(GlobalSettings.CurrentProject.FileName)) && MessageBox.Show("Ohne Speichern beenden?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.No)
                        e.Cancel = true;
                }
            }

            // Wenn das Programm nicht beendet wird, muss die Konfigurationsdatei nicht gespeichert werden.
            if (!e.Cancel)
                // Konfigurationsdatei speichern erfolgreich?
                if (!GlobalSettings.SaveToFile("config.xml"))
                    // Trotz Fehler beenden?
                    if (MessageBox.Show("Soll trotzdem beendet werden?", "Beenden", MessageBoxButtons.YesNo) == DialogResult.No)
                        // Beenden abbrechen!
                        e.Cancel = true;
        }

        void MainWindow_Load(object sender, EventArgs e)
        {
            // Graph erstellen
            graph = new Graph();
            // Zoom Funktionalität auf das Mausradevent registrieren.
            MouseWheel += (s, ev) => graph.AddZoom(Math.Sign(ev.Delta));
            // Controls für den Graphen setzen!
            graph.SetParentPanel(graphPanel);

            // Kontrollfenster initialisieren.
            controlWindow = new ControlWindow();
            // Das Hauptfenster als "Eigentümer" vom Kontrollfenster setzen.
            // Wenn das Hauptfenster in den Vordergrund gebracht wird, kommt das Kontrollfenster mit nach vor.
            AddOwnedForm(controlWindow);
            // Kontrollfenster positionieren.
            controlWindow.Left = Left + Width + 20;
            controlWindow.Top = Top;

            // Vorschau aktivieren/deaktivieren
            controlWindow.ShowPreview = GlobalSettings.EnableControlWindowPreview;
         }

        private void commandLineExecuteButton_Click(object sender, EventArgs e)
        {
            try
            {
                CommandInterpreter.InterpretCommand(commandLineTextBox.Text, false, this);
                commandLineTextBox.Text = "";
                SetUnsaved();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void commandLineTextBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab && GlobalSettings.EnableTabCompletion)
                e.IsInputKey = true;
        }

        private void commandLineTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter)
            {
                commandLineExecuteButton.PerformClick();
            }
            else if (e.KeyCode == Keys.Tab)
            {
                string[] commands = new string[] { "ADDWAVE", "DELETEWAVE", "CHANGEWAVE", "LIST" };

                foreach (string s in commands)
                {
                    string controlString = commandLineTextBox.Text.ToUpper().Split(' ')[0];

                    if (!s.StartsWith(controlString) && !controlString.StartsWith(s))
                        continue;

                    string[] found = commands.ToList().FindAll((c) => c.StartsWith(controlString) && c != s).ToArray();

                    if (s.StartsWith(controlString) && found.Length == 0 && controlString != s)
                        switch (s)
                        {
                            case "ADDWAVE":
                                    commandLineTextBox.Text = "ADDWAVE -o <offset[ms]> -d <duration[ms]> -a <amplitude[%]> -f <frequency[Hz]> -n <name>";
                                break;

                            case "DELETEWAVE":
                                    commandLineTextBox.Text = "DELETEWAVE -n <name>";
                                break;

                            case "CHANGEWAVE":
                                commandLineTextBox.Text = "CHANGEWAVE -n <nametochange> -o <offset[ms]> -d <duration[ms]> -a <amplitude[%]> -f <frequency[Hz]>";
                                break;

                            case "LIST":
                                commandLineTextBox.Text = "LIST";
                                break;
                        }

                startSelecting:
                    int start = 0;
                    int end = 0;

                    if (!e.Shift)
                    {
                        start = commandLineTextBox.Text.IndexOf('<', commandLineTextBox.SelectionStart);
                        if (start == commandLineTextBox.SelectionStart && commandLineTextBox.Text.Length >= start + 1)
                            start = commandLineTextBox.Text.IndexOf('<', commandLineTextBox.SelectionStart + 1);

                        if (start == -1)
                            end = -1;
                        else
                            end = commandLineTextBox.Text.IndexOf('>', start);

                        if (start == -1)
                            start = 0;
                    }
                    else
                    {
                        if (commandLineTextBox.SelectionStart == 0)
                            commandLineTextBox.SelectionStart = commandLineTextBox.Text.Length - 1;

                        start = commandLineTextBox.Text.LastIndexOf('<', commandLineTextBox.SelectionStart - 1, commandLineTextBox.SelectionStart - 1);

                        if (start == -1)
                            end = -1;
                        else
                            end = commandLineTextBox.Text.IndexOf('>', start);

                        if (start == -1)
                            start = 0;
                    }

                    commandLineTextBox.Select(start, end - start + 1);

                    if (!commandLineTextBox.Text.Contains('<') || !commandLineTextBox.Text.Contains('>'))
                        break;

                    if (end == -1)
                        goto startSelecting;
                }
            }
        }
        
        private void commandLineTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CommandInterpreter.InterpretCommand(commandLineTextBox.Text, true, this);
            }
            catch
            {
                // Bei der Vorschau sind Fehler egal, denn im Fehlerfall wird sowieso nichts angezeigt.
            }
        }

        #endregion


        #region FileMenu

        void newMenuItem_Click(object sender, EventArgs e)
        {
            // Wenn bereits ein Projekt geöffnet ist (ungespeichert!)...
            if (statusLabel.Text.StartsWith("*"))
                if (MessageBox.Show("Soll das geöffnete Projekt gespeichert werden?", "Speichern", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    saveProjectAsMenuItem_Click(sender, e);
                    // Wenn das Speichern nicht erfolgreich war, abbrechen!
                    if (statusLabel.Text.StartsWith("*"))
                        return;
                }

            Storage.Waves = new List<Wave>();

            graph.RemoveData(Storage.WavFile);
            Storage.WavFile = null;
            graph.ClearData();
            // neues Projekt erstellen
            GlobalSettings.CurrentProject = new ProjectSettings();
            // Statustext setzen
            statusLabel.Text = "*" + Path.ChangeExtension(GlobalSettings.DefaultProjectFileName, ".wvprj");
            // Dateiname setzen
            GlobalSettings.CurrentProject.FileName = Path.GetFullPath(statusLabel.Text.Trim('*'));
            // Graphen das Projekt zuweisen
            graph.SetProjectSettings(GlobalSettings.CurrentProject);

            // Kontrollfenster anzeigen, wenn die Option gesetzt ist
            if (GlobalSettings.ShowControlWindow)
                controlWindow.Show();
            // Kommandozeile anzeigen, wenn die Option gesetzt ist
            if (GlobalSettings.ShowCommandLine)
                commandLinePanel.Show();

            // Aktiviert Speicher- und Exportbuttons
            saveProjectAsMenuItem.Enabled = saveProjectMenuItem.Enabled = projectsettingsToolStripMenuItem.Enabled = exportWAVMenuItem.Enabled = importWAVToolStripMenuItem.Enabled = true;

            // Kontrollfenster neu laden
            controlWindow.ReloadWaves();
        }

        void openMenuItem_Click(object sender, EventArgs e)
        {
            // OpenFileDialog erstllen
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "WAVEing - Datei|*.wvprj";

            // Startverzeichnis setzen
            od.InitialDirectory = GlobalSettings.DefaultProjectFileDirectory;


            if (od.ShowDialog() == DialogResult.OK)
            {
                // Ausgewähltes Objekt öffnen
                GlobalSettings.CurrentProject = ProjectSettings.Load(od.FileName);
                if (GlobalSettings.CurrentProject == null)
                    return;

                statusLabel.Text = Path.GetFileName(GlobalSettings.CurrentProject.FileName);
                //Graphen das Projekt zuweisen
                graph.SetProjectSettings(GlobalSettings.CurrentProject);

                // Kontrollfenster anzeigen, wenn die Option gesetzt ist
                if (GlobalSettings.ShowControlWindow)
                    controlWindow.Show();
                // Kommandozeile anzeigen, wenn die Option gesetzt ist
                if (GlobalSettings.ShowCommandLine)
                    commandLinePanel.Show();

                // Aktiviert Speicher- und Exportbuttons
                saveProjectAsMenuItem.Enabled = saveProjectMenuItem.Enabled = projectsettingsToolStripMenuItem.Enabled = exportWAVMenuItem.Enabled = importWAVToolStripMenuItem.Enabled = true;

                // Kontrollfenster neu laden
                controlWindow.ReloadWaves();
            }
        }


        void saveProjectMenuItem_Click(object sender, EventArgs e)
        {
            // Wenn kein Projekt vorhanden ist, abbrechen.
            if (GlobalSettings.CurrentProject == null)
                return;

            // '*' im Statustext zeigt ein ungespeichertes Projekt an
            if (statusLabel.Text.StartsWith("*") && statusLabel.Text.Trim('*') != Path.ChangeExtension(GlobalSettings.DefaultProjectFileName, ".wvprj"))
            {
                // Wenn das Speichern erfolgreich war, wird der Statustext neu gesetzt.
                if (GlobalSettings.CurrentProject.Save())
                    statusLabel.Text = Path.GetFileName(GlobalSettings.CurrentProject.FileName);
            }
            else if (!File.Exists(Path.GetFullPath(GlobalSettings.CurrentProject.FileName)) || statusLabel.Text.Trim('*') == Path.ChangeExtension(GlobalSettings.DefaultProjectFileName, ".wvprj"))
                // Wenn das Projekt noch nicht gespeichert wurde, Dialog aufrufen!
                saveProjectAsMenuItem_Click(sender, e);
        }

        void saveProjectAsMenuItem_Click(object sender, EventArgs e)
        {
            // Wenn kein Projekt vorhanden ist, abbrechen.
            if (GlobalSettings.CurrentProject == null)
                return;

            // Projekt speichern
            SaveFileDialog sd = new SaveFileDialog();

            // Wenn das Projekt schon gespeichert wurde, Startverzeichnis auf den Pfad dieser Datei legen.
            if (File.Exists(GlobalSettings.CurrentProject.FileName))
            {
                // Wenn schon eine Datei existiert, den Dateinamen setzen...
                sd.FileName = Path.GetFileNameWithoutExtension(GlobalSettings.CurrentProject.FileName);
                // ... und das Verzeichnis der Datei, als Startordner anführen.
                sd.InitialDirectory = Path.GetDirectoryName(GlobalSettings.CurrentProject.FileName);
            }
            else
                // Ansonsten das Standardverzeichnis wählen
                sd.InitialDirectory = GlobalSettings.DefaultProjectFileDirectory;

            sd.Filter = "WAVEing - Datei|*.wvprj";

            if (sd.ShowDialog() == DialogResult.OK)
            {
                // Dateinamen setzen
                GlobalSettings.CurrentProject.FileName = Path.GetFullPath(sd.FileName);
                // Wenn das Speichern erfolgreich war, den Statustext neu setzen.
                if (GlobalSettings.CurrentProject.Save())
                    statusLabel.Text = Path.GetFileName(sd.FileName);
            }
        }

        void importWAVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // OpenFileDialog erstllen
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "WAVE - Audio Datei|*.wav";

            // Startverzeichnis setzen
            od.InitialDirectory = GlobalSettings.DefaultProjectFileDirectory;


            if (od.ShowDialog() == DialogResult.OK)
            {
                RawData data = WaveFileGenerator.LoadFromFile(od.FileName);

                if (data == null)
                {
                    MessageBox.Show("Invalid wav file!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Storage.WavFile = data;
                controlWindow.ReloadWaves();
                InvalidateGraph();
            }
        }

        void exportWAVMenuItem_Click(object sender, EventArgs e)
        {
            saveProjectMenuItem.PerformClick();

            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "WAVE - Audio Datei|*.wav";

            sd.InitialDirectory = GlobalSettings.DefaultProjectFileDirectory;
            sd.FileName = Path.GetFileNameWithoutExtension(GlobalSettings.CurrentProject.FileName);

            if (sd.ShowDialog() == DialogResult.OK)
            {
                ExportDialog d = new ExportDialog(sd.FileName);
                AddOwnedForm(d);
                d.StartPosition = FormStartPosition.CenterParent;

                d.ShowDialog();

                RemoveOwnedForm(d);                
            }
        }


        void exitMenuItem_Click(object sender, EventArgs e)
        {
            // Schließen anfordern
            Close();
        }

        #endregion

        #region ViewMenu

        void showControlWindowMenuItem_Click(object sender, EventArgs e)
        {
            GlobalSettings.ShowControlWindow = (sender as ToolStripMenuItem).Checked;

            if (GlobalSettings.CurrentProject != null)
            {
                // positionieren des Kontrollfensters
                controlWindow.Left = Left + Width + 20;
                controlWindow.Top = Top;

                // Kontrollfenster ein-/ausblenden
                if (GlobalSettings.ShowControlWindow)
                {
                    controlWindow.Show();
                    controlWindow.ShowPreview = GlobalSettings.EnableControlWindowPreview;
                }
                else
                {
                    controlWindow.Hide();
                    controlWindow.ShowPreview = false;
                }
                // Fokus auf das Hauptformular setzen
                Focus();
            }
        }

        void showCommandlineMenuItem_Click(object sender, EventArgs e)
        {
            GlobalSettings.ShowCommandLine = (sender as ToolStripMenuItem).Checked;

            if (GlobalSettings.CurrentProject != null)
            {
                // Kommandozeile ein-/ausblenden
                if (GlobalSettings.ShowCommandLine)
                {
                    commandLinePanel.Show();

                    if (GlobalSettings.EnableCommandLinePreview)
                        CommandInterpreter.InterpretCommand(commandLineTextBox.Text, true, this);
                }
                else
                {
                    commandLinePanel.Hide();
                    CommandInterpreter.ClearPreview();
                }
                // Graph neu zeichnen aufgrund der veränderten Zeichenfläche (Kommandozeile ausgeblendet)!
                graph.ClearData();
                graph.AddDataRange(Storage.Waves.Cast<ITimeValues>().ToList());
                InvalidateGraph();
            }
        }

        #endregion

        #region SettingsMenu

        void commandlineTabCompletionMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            // Einstellungen neu setzen.
            GlobalSettings.EnableTabCompletion = (sender as ToolStripMenuItem).Checked;
        }

        void commandlinePreviewMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            // Einstellungen neu setzen.
            GlobalSettings.EnableCommandLinePreview = (sender as ToolStripMenuItem).Checked;
        }

        void projectsettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Wenn kein Projekt vorhanden ist, abbrechen.
            if (GlobalSettings.CurrentProject == null)
            {
                MessageBox.Show("Du musst zuerst ein Projekt erstellen!");
                return;
            }

            // Checken, ob eine externe Datei festgelegt ist.
            string rawDataPath = Storage.WavFile == null ? null : Storage.WavFile.FileName;
            // Dialog erstellen.
            ProjectSettingsDialog psd = new ProjectSettingsDialog();

            // Wenn auf "Save" gedrückt wurde.
            if (psd.ShowDialog() == DialogResult.OK)
            {
                //Einstellungen setzen.
                GlobalSettings.CurrentProject.BitsPerSample = psd.BitsPerSample;
                GlobalSettings.CurrentProject.Channels = psd.AudioChannels;
                GlobalSettings.CurrentProject.SampleRate = psd.SampleRate;
                GlobalSettings.CurrentProject.Format = psd.Format;
                graph.RemoveData(Storage.WavFile);
                Storage.WavFile = psd.RawDataPath != "" ? new RawData(psd.RawDataPath) : null;
                GlobalSettings.CurrentProject.CutOverflow = psd.CutOverflow;

                // Status Text mit einem * versehen (nicht gespeichert!)
                SetUnsaved();

                // Graphen neu berechnen/zeichnen
                graph.SetProjectSettings(GlobalSettings.CurrentProject);

                controlWindow.ReloadWaves();
                InvalidateGraph();
            }

        }

        void moreSettingsMenuItem_Click(object sender, EventArgs e)
        {
            // Standardeinstellungsdialog erzeugen.
            MoreSettings ms = new MoreSettings();
            if (ms.ShowDialog() == DialogResult.OK)
            {
                // Einstellungen setzen
                GlobalSettings.DefaultProjectFileName = ms.ProjectFileName;
                GlobalSettings.DefaultProjectFileDirectory = ms.ProjectDirectory;

                GlobalSettings.DefaultBitsPerSample = ms.BitsPerSample;
                GlobalSettings.DefaultChannels = ms.AudioChannels;
                GlobalSettings.DefaultFormat = ms.Format;
                GlobalSettings.DefaultSampleRate = ms.SampleRate;
            }
        }

        private void helpMenuItem_Click(object sender, EventArgs e)
        {
            HelpDialog hd = new HelpDialog();
            hd.ShowDialog();
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            AboutDialog ad = new AboutDialog();
            
            ad.ShowDialog();
        }
       
        #endregion


        #region HelpFunctions

        /// <summary>
        /// Zwingt den Graphen, sich neu zu zeichnen.
        /// </summary>
        public void InvalidateGraph()
        {
            //Graph neu zeichnen!
            if (graph != null)
                foreach (Control c in graphPanel.Controls)
                    if (typeof(Panel) == c.GetType())
                        c.Invalidate();
        }

        /// <summary>
        /// Signalisiert ungespeicherte Änderungen.
        /// </summary>
        public void SetUnsaved()
        {
            // Signalisiert dem Programm durch '*', dass das Projekt nicht gespeichert wurde.
            statusLabel.Text = '*' + statusLabel.Text.Trim('*');
        }

        /// <summary>
        /// Gibt den Graphen der Instanz zurück.
        /// </summary>
        /// <returns>Der Graph der Instanz</returns>
        public Graph GetGraph()
        {
            return graph;
        }

        #endregion

        private void commandLineTextBox_Click(object sender, EventArgs e)
        {
            controlWindow.ShowPreview = false;
        }
    }
}
