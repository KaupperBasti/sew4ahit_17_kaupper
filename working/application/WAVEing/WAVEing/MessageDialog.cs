﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
     /// <summary>
     /// Generisches Nachrichtenfenster 
     /// </summary>
    public partial class MessageDialog : Form
    {
        public MessageDialog(string message, string caption)
        {
            InitializeComponent();
            this.Text = caption;

            richTextBox1.Text = message;
            richTextBox1.Font = GlobalSettings.DefaultFont;
        }

        private void MessageDialog_KeyDown(object sender, KeyEventArgs e)
        {
            Close();
        }
    }
}
