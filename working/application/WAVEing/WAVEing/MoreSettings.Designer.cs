﻿namespace WAVEing
{
    partial class MoreSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonsPanel = new System.Windows.Forms.Panel();
            this.canelProjectSettingsButton = new System.Windows.Forms.Button();
            this.saveProjectSettingsButton = new System.Windows.Forms.Button();
            this.formatDropDown = new System.Windows.Forms.ComboBox();
            this.channelDropDown = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.projectFileName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.frequencyPanel = new System.Windows.Forms.Panel();
            this.sampleRateNumeric = new System.Windows.Forms.NumericUpDown();
            this.sampleRateLabel = new System.Windows.Forms.Label();
            this.propertiesPanel2 = new System.Windows.Forms.Panel();
            this.separator3 = new System.Windows.Forms.Panel();
            this.amplitudePanel = new System.Windows.Forms.Panel();
            this.bpsDropDown = new System.Windows.Forms.ComboBox();
            this.bpsLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.projectFileDirectory = new System.Windows.Forms.TextBox();
            this.projectFileDirectoryButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonsPanel.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.frequencyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sampleRateNumeric)).BeginInit();
            this.propertiesPanel2.SuspendLayout();
            this.amplitudePanel.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonsPanel
            // 
            this.buttonsPanel.Controls.Add(this.canelProjectSettingsButton);
            this.buttonsPanel.Controls.Add(this.saveProjectSettingsButton);
            this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonsPanel.Location = new System.Drawing.Point(0, 200);
            this.buttonsPanel.Name = "buttonsPanel";
            this.buttonsPanel.Size = new System.Drawing.Size(276, 32);
            this.buttonsPanel.TabIndex = 20;
            // 
            // canelProjectSettingsButton
            // 
            this.canelProjectSettingsButton.BackColor = System.Drawing.Color.White;
            this.canelProjectSettingsButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.canelProjectSettingsButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.canelProjectSettingsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.canelProjectSettingsButton.FlatAppearance.BorderSize = 0;
            this.canelProjectSettingsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.canelProjectSettingsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.canelProjectSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.canelProjectSettingsButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.canelProjectSettingsButton.Location = new System.Drawing.Point(134, 0);
            this.canelProjectSettingsButton.Name = "canelProjectSettingsButton";
            this.canelProjectSettingsButton.Size = new System.Drawing.Size(142, 32);
            this.canelProjectSettingsButton.TabIndex = 7;
            this.canelProjectSettingsButton.Text = "Cancel";
            this.canelProjectSettingsButton.UseVisualStyleBackColor = false;
            // 
            // saveProjectSettingsButton
            // 
            this.saveProjectSettingsButton.BackColor = System.Drawing.Color.White;
            this.saveProjectSettingsButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.saveProjectSettingsButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.saveProjectSettingsButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.saveProjectSettingsButton.FlatAppearance.BorderSize = 0;
            this.saveProjectSettingsButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.saveProjectSettingsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.saveProjectSettingsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveProjectSettingsButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.saveProjectSettingsButton.Location = new System.Drawing.Point(0, 0);
            this.saveProjectSettingsButton.Name = "saveProjectSettingsButton";
            this.saveProjectSettingsButton.Size = new System.Drawing.Size(142, 32);
            this.saveProjectSettingsButton.TabIndex = 6;
            this.saveProjectSettingsButton.Text = "Save";
            this.saveProjectSettingsButton.UseVisualStyleBackColor = false;
            // 
            // formatDropDown
            // 
            this.formatDropDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.formatDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.formatDropDown.FormattingEnabled = true;
            this.formatDropDown.Location = new System.Drawing.Point(0, 17);
            this.formatDropDown.Name = "formatDropDown";
            this.formatDropDown.Size = new System.Drawing.Size(100, 21);
            this.formatDropDown.TabIndex = 3;
            // 
            // channelDropDown
            // 
            this.channelDropDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.channelDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.channelDropDown.FormattingEnabled = true;
            this.channelDropDown.Location = new System.Drawing.Point(0, 17);
            this.channelDropDown.Name = "channelDropDown";
            this.channelDropDown.Size = new System.Drawing.Size(100, 21);
            this.channelDropDown.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kanäle";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Audioformat";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Project Name:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.projectFileName);
            this.panel5.Location = new System.Drawing.Point(12, 147);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(260, 16);
            this.panel5.TabIndex = 23;
            // 
            // projectFileName
            // 
            this.projectFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectFileName.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.projectFileName.Location = new System.Drawing.Point(80, 1);
            this.projectFileName.Name = "projectFileName";
            this.projectFileName.Size = new System.Drawing.Size(180, 16);
            this.projectFileName.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.formatDropDown);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(160, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(100, 38);
            this.panel2.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Location = new System.Drawing.Point(12, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 38);
            this.panel1.TabIndex = 22;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(100, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 38);
            this.panel3.TabIndex = 9;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.channelDropDown);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(100, 38);
            this.panel4.TabIndex = 8;
            // 
            // frequencyPanel
            // 
            this.frequencyPanel.BackColor = System.Drawing.SystemColors.Control;
            this.frequencyPanel.Controls.Add(this.sampleRateNumeric);
            this.frequencyPanel.Controls.Add(this.sampleRateLabel);
            this.frequencyPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.frequencyPanel.Location = new System.Drawing.Point(160, 0);
            this.frequencyPanel.Name = "frequencyPanel";
            this.frequencyPanel.Size = new System.Drawing.Size(100, 38);
            this.frequencyPanel.TabIndex = 14;
            // 
            // sampleRateNumeric
            // 
            this.sampleRateNumeric.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.sampleRateNumeric.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.sampleRateNumeric.Location = new System.Drawing.Point(0, 18);
            this.sampleRateNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.sampleRateNumeric.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.sampleRateNumeric.Name = "sampleRateNumeric";
            this.sampleRateNumeric.Size = new System.Drawing.Size(100, 20);
            this.sampleRateNumeric.TabIndex = 1;
            this.sampleRateNumeric.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // sampleRateLabel
            // 
            this.sampleRateLabel.AutoSize = true;
            this.sampleRateLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sampleRateLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sampleRateLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.sampleRateLabel.Location = new System.Drawing.Point(0, 0);
            this.sampleRateLabel.Name = "sampleRateLabel";
            this.sampleRateLabel.Size = new System.Drawing.Size(93, 16);
            this.sampleRateLabel.TabIndex = 4;
            this.sampleRateLabel.Text = "Samplingrate";
            this.sampleRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // propertiesPanel2
            // 
            this.propertiesPanel2.Controls.Add(this.frequencyPanel);
            this.propertiesPanel2.Controls.Add(this.separator3);
            this.propertiesPanel2.Controls.Add(this.amplitudePanel);
            this.propertiesPanel2.Location = new System.Drawing.Point(12, 42);
            this.propertiesPanel2.Name = "propertiesPanel2";
            this.propertiesPanel2.Size = new System.Drawing.Size(260, 38);
            this.propertiesPanel2.TabIndex = 21;
            // 
            // separator3
            // 
            this.separator3.BackColor = System.Drawing.SystemColors.Control;
            this.separator3.Dock = System.Windows.Forms.DockStyle.Left;
            this.separator3.Location = new System.Drawing.Point(100, 0);
            this.separator3.Name = "separator3";
            this.separator3.Size = new System.Drawing.Size(3, 38);
            this.separator3.TabIndex = 9;
            // 
            // amplitudePanel
            // 
            this.amplitudePanel.Controls.Add(this.bpsDropDown);
            this.amplitudePanel.Controls.Add(this.bpsLabel);
            this.amplitudePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.amplitudePanel.Location = new System.Drawing.Point(0, 0);
            this.amplitudePanel.Name = "amplitudePanel";
            this.amplitudePanel.Size = new System.Drawing.Size(100, 38);
            this.amplitudePanel.TabIndex = 8;
            // 
            // bpsDropDown
            // 
            this.bpsDropDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bpsDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bpsDropDown.FormattingEnabled = true;
            this.bpsDropDown.Location = new System.Drawing.Point(0, 17);
            this.bpsDropDown.Name = "bpsDropDown";
            this.bpsDropDown.Size = new System.Drawing.Size(100, 21);
            this.bpsDropDown.TabIndex = 0;
            // 
            // bpsLabel
            // 
            this.bpsLabel.AutoSize = true;
            this.bpsLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.bpsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bpsLabel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.bpsLabel.Location = new System.Drawing.Point(0, 0);
            this.bpsLabel.Name = "bpsLabel";
            this.bpsLabel.Size = new System.Drawing.Size(110, 16);
            this.bpsLabel.TabIndex = 3;
            this.bpsLabel.Text = "Bits pro Sample";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.projectFileDirectory);
            this.panel6.Controls.Add(this.projectFileDirectoryButton);
            this.panel6.Location = new System.Drawing.Point(12, 178);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(260, 16);
            this.panel6.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(-3, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Speicherort:";
            // 
            // projectFileDirectory
            // 
            this.projectFileDirectory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projectFileDirectory.Enabled = false;
            this.projectFileDirectory.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.projectFileDirectory.Location = new System.Drawing.Point(80, 1);
            this.projectFileDirectory.Name = "projectFileDirectory";
            this.projectFileDirectory.Size = new System.Drawing.Size(140, 16);
            this.projectFileDirectory.TabIndex = 17;
            this.projectFileDirectory.Text = "C:\\";
            // 
            // projectFileDirectoryButton
            // 
            this.projectFileDirectoryButton.BackColor = System.Drawing.Color.White;
            this.projectFileDirectoryButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.projectFileDirectoryButton.FlatAppearance.BorderSize = 0;
            this.projectFileDirectoryButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.projectFileDirectoryButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.projectFileDirectoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.projectFileDirectoryButton.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.projectFileDirectoryButton.Location = new System.Drawing.Point(223, -4);
            this.projectFileDirectoryButton.Margin = new System.Windows.Forms.Padding(0);
            this.projectFileDirectoryButton.Name = "projectFileDirectoryButton";
            this.projectFileDirectoryButton.Size = new System.Drawing.Size(37, 23);
            this.projectFileDirectoryButton.TabIndex = 5;
            this.projectFileDirectoryButton.Text = "...";
            this.projectFileDirectoryButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.projectFileDirectoryButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.projectFileDirectoryButton.UseVisualStyleBackColor = false;
            this.projectFileDirectoryButton.Click += new System.EventHandler(this.projectFileDirectoryButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.label5.Location = new System.Drawing.Point(69, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 16);
            this.label5.TabIndex = 18;
            this.label5.Text = "Standardeinstellungen";
            // 
            // MoreSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 232);
            this.ControlBox = false;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.buttonsPanel);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.propertiesPanel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MoreSettings";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Weitere Einstellungen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MoreSettings_FormClosing);
            this.buttonsPanel.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.frequencyPanel.ResumeLayout(false);
            this.frequencyPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sampleRateNumeric)).EndInit();
            this.propertiesPanel2.ResumeLayout(false);
            this.amplitudePanel.ResumeLayout(false);
            this.amplitudePanel.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel buttonsPanel;
        private System.Windows.Forms.Button canelProjectSettingsButton;
        private System.Windows.Forms.Button saveProjectSettingsButton;
        private System.Windows.Forms.ComboBox formatDropDown;
        private System.Windows.Forms.ComboBox channelDropDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox projectFileName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel frequencyPanel;
        private System.Windows.Forms.NumericUpDown sampleRateNumeric;
        private System.Windows.Forms.Label sampleRateLabel;
        private System.Windows.Forms.Panel propertiesPanel2;
        private System.Windows.Forms.Panel separator3;
        private System.Windows.Forms.Panel amplitudePanel;
        private System.Windows.Forms.ComboBox bpsDropDown;
        private System.Windows.Forms.Label bpsLabel;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox projectFileDirectory;
        private System.Windows.Forms.Button projectFileDirectoryButton;
        private System.Windows.Forms.Label label5;
    }
}