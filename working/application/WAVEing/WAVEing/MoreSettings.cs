﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    public partial class MoreSettings : Form
    {
        public string ProjectFileName { get { return projectFileName.Text; } }
        public string ProjectDirectory { get { return projectFileDirectory.Text;  } }
        public BPS BitsPerSample { get { return (BPS)bpsDropDown.SelectedItem;  } }
        public Formats Format { get { return (Formats)formatDropDown.SelectedItem; } }
        public int SampleRate { get { return decimal.ToInt32(sampleRateNumeric.Value); } }
        public Channels AudioChannels { get { return (Channels)channelDropDown.SelectedItem; } }

        public MoreSettings()
        {
            InitializeComponent();

            // Initialisiert die DropDownBoxen mit den Werten aus den Enumerationen und setzt die Startwerte für alle Boxen.
            foreach (BPS bps in Enum.GetValues(typeof(BPS)).Cast<BPS>())
                bpsDropDown.Items.Add(bps);
            bpsDropDown.SelectedItem = GlobalSettings.DefaultBitsPerSample;

            foreach (Channels ch in Enum.GetValues(typeof(Channels)).Cast<Channels>())
                channelDropDown.Items.Add(ch);
            channelDropDown.SelectedItem = GlobalSettings.DefaultChannels;
            foreach (Formats f in Enum.GetValues(typeof(Formats)).Cast<Formats>())
                formatDropDown.Items.Add(f);
            formatDropDown.SelectedItem = GlobalSettings.DefaultFormat;

            sampleRateNumeric.Value = GlobalSettings.DefaultSampleRate;
            projectFileName.Text = GlobalSettings.DefaultProjectFileName;
            projectFileDirectory.Text = GlobalSettings.DefaultProjectFileDirectory;
        }

        private void projectFileDirectoryButton_Click(object sender, EventArgs e)
        {
            // Öffnet einen Dialog um den Standardpfad auszuwählen (ev. vom bereits gesetzten Verzeichnis weg!)
            FolderBrowserDialog fd = new FolderBrowserDialog();

            fd.RootFolder = Environment.SpecialFolder.Desktop;
            fd.SelectedPath = Directory.Exists(GlobalSettings.DefaultProjectFileDirectory) ? GlobalSettings.DefaultProjectFileDirectory : Environment.CurrentDirectory;
            fd.ShowNewFolderButton = true;

            if (fd.ShowDialog() == DialogResult.OK)
                projectFileDirectory.Text = Path.GetDirectoryName(fd.SelectedPath);
        }

        private void MoreSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Bereinigt den Dateinamen von unzulässigen Zeichen und setzt die korrekte Dateinamenerweiterung
            Path.GetInvalidFileNameChars().ToList().ForEach((c)=>projectFileName.Text=projectFileName.Text.Replace(c.ToString(),  ""));
            Path.GetInvalidPathChars().ToList().ForEach((c)=>projectFileName.Text=projectFileName.Text.Replace(c.ToString(),  ""));
            projectFileName.Text = Path.ChangeExtension(projectFileName.Text, ".wvprj");


            // Prüft noch einmal alle Daten auf Validität
            if (bpsDropDown.SelectedIndex == -1)
                e.Cancel = true;
            else if (!Directory.Exists(projectFileDirectory.Text))
            {
                MessageBox.Show("Ordner existiert nicht!");
                e.Cancel = true;
            }
            else if (projectFileName.Text == "")
            {
                MessageBox.Show("Kein Dateiname angegeben!");
                e.Cancel = true;
            }
            else if (channelDropDown.SelectedIndex == -1)
                e.Cancel = true;
            else if (formatDropDown.SelectedIndex == -1)
                e.Cancel = true;
        }
    }
}
