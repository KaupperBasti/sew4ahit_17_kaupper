﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace WAVEing
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string param = null;

            if (args.Length == 1)
                param = args[0];

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow(param));
        }
    }
}
