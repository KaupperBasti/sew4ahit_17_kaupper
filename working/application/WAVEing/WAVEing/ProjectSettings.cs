﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace WAVEing
{
    public class ProjectSettings : WaveSettings
    {
        //Name des Projektes
        public string FileName { get; set; }
        //Überflussschutz aus?
        public bool CutOverflow { get; set; }


        public ProjectSettings() : base()
        {
            FileName = "";
            CutOverflow = true;
        }

        public ProjectSettings(Channels chs, BPS bps, int sampleRate, Formats f, bool cutOverflow, string name)
            : base(chs, bps, sampleRate, f)
        {
            FileName = name;
            CutOverflow = cutOverflow;
        }

        /// <summary>
        /// Speichert diese Instanz der Projekteinstellungen.
        /// </summary>
        /// <returns>Konnte die Datei erfolgreich gespeichert werden?</returns>
        public bool Save()
        {
            XmlWriter wr;

            // Garantiert, dass die Datei die Endung ".wvprj" hat.
            GlobalSettings.CurrentProject.FileName = Path.ChangeExtension(GlobalSettings.CurrentProject.FileName, ".wvprj");
            
            try
            {
                // Erstellt den XmlWriter.
                wr = XmlWriter.Create(FileName);
            }
            catch
            {
                MessageBox.Show("Fehler beim Speichern der Konfigurationsdatei!\nDateipfad nicht erkannt!");
                return false;
            }

            // Schreiben der XML-Datei.
            // Aufbau der Datei: siehe it.pdf

            wr.WriteStartDocument();

            wr.WriteRaw("\r\n");
            wr.WriteStartElement("wvprj");
            wr.WriteAttributeString("name", Path.GetFileNameWithoutExtension(FileName));

            wr.WriteRaw("\r\n\t");
            wr.WriteComment("Externe Abhängigkeiten");

            wr.WriteRaw("\r\n\t");
            wr.WriteComment("falls eine .wav-Datei dem Projekt zu Grunde liegt");


            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("dependency");
            wr.WriteString(Storage.WavFile != null ? Storage.WavFile.FileName : "");
            wr.WriteEndElement();


            wr.WriteRaw("\r\n\r\n\t");
            wr.WriteComment("Allgemeine Projekteinstellungen");

            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("settings");

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "samplerate");
            wr.WriteString(SampleRate.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "bitspersample");
            wr.WriteString(((int)BitsPerSample).ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "channels");
            wr.WriteString(((int)Channels).ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t\t");
            wr.WriteStartElement("value");
            wr.WriteAttributeString("name", "overflow");
            wr.WriteString(CutOverflow.ToString());
            wr.WriteEndElement();

            wr.WriteRaw("\r\n\t");
            wr.WriteEndElement();


            wr.WriteRaw("\r\n\r\n\t");
            wr.WriteComment("Die selbsterzeugten Audiodaten");
            
            wr.WriteRaw("\r\n\t");
            wr.WriteStartElement("data");

            foreach (Wave w in Storage.Waves)
            {
                wr.WriteRaw("\r\n\t\t");
                wr.WriteStartElement("wave");
                wr.WriteAttributeString("name", w.Name);
                wr.WriteAttributeString("offset", w.Offset.ToString());
                wr.WriteAttributeString("duration", w.Duration.ToString());
                wr.WriteAttributeString("frequency", w.Frequency.ToString());
                wr.WriteAttributeString("amplitude", w.Amplitude.ToString());
                
            }

            wr.WriteRaw("\r\n\t");
            wr.WriteEndElement();
            
            wr.WriteRaw("\r\n");
            wr.WriteEndElement();

            wr.Close();

            return true;
        }













        /// <summary>
        /// Lädt ein bereits bestehendes Projekt (.wvprj)
        /// </summary>
        /// <param name="projectName">Relativer Dateipfad der Projektdatei.</param>
        /// <returns>Gibt ein Objekt der Klasse ProjectSettings zurück. Bei einem Fehler wird null zurückgegeben.</returns>
        public static ProjectSettings Load(string projectName)
        {
            // Eine leere ProjectSettings Klasse wird erstellt.
            ProjectSettings settings = new ProjectSettings();

            // Die Liste der Schwingungen wird neu erstellt.
            Storage.Waves = new List<Wave>();


            FileStream fs = null;
            // Prüft, ob die Datei vorhanden ist.
            if (!File.Exists(projectName))
                return null;


            XmlReader re = null;
            string currentElementName = "";
            try
            {
                // XmlReader wird initialisiert
                // Ohne den FileStream, kann die Datei anschließend nicht ordnungsgemäß geschlossen werden!
                re = XmlReader.Create(fs = new FileStream(projectName, FileMode.Open));


                // Iteriert durch alle Knoten der XML-Datei
                // Aufbau der Datei: siehe it.pdf!
                while (re.Read())
                {
                    switch (re.NodeType)
                    {
                        //Falls der Knoten ein XML-Tag ist
                        case XmlNodeType.Element:
                            switch (re.Name)
                            {
                                // Der Starttag "wvprj" enthält nochmals den Namen des Projektes (wird nicht gebraucht)
                                case "wvprj":
                                    //settings.FileName = re.GetAttribute("name");
                                    break;
                                // Der Tag "dependency" enthält den Pfad der externen Audiodatei (falls es eine gibt!)
                                case "dependency":
                                    currentElementName = "dependency";
                                    break;
                                // Wenn ein Wert erwartet wird, muss der Optionsname zwischengespeichert werden.
                                case "value":
                                    currentElementName = re.GetAttribute("name");
                                    break;
                                // Der Tag "wave" enthält direkt alle benötigten Attribute um eine Klasse zu erzeugen.
                                case "wave":
                                    // Parsingfehler werden (oberflächlich) durch das try-catch-Konstrukt abgefangen.
                                    double duration = double.Parse(re.GetAttribute("duration"));
                                    double offset = double.Parse(re.GetAttribute("offset"));
                                    double amplitude = double.Parse(re.GetAttribute("amplitude"));
                                    double frequency = double.Parse(re.GetAttribute("frequency"));
                                    string name = re.GetAttribute("name");

                                    Storage.Waves.Add(new Wave(name, offset, duration, frequency, amplitude));
                                    break;
                            }
                            break;

                        // Falls der Knoten eine Textzeile ist, werden die Werte dem entsprechenden Optionen zugeteilt.
                        // Das (oberflächliche) Exceptionhandling übernimmt das try-catch.
                        case XmlNodeType.Text:
                            switch (currentElementName)
                            {
                                case "dependency":
                                    if (re.Value != "")
                                        Storage.WavFile = new RawData(re.Value);
                                    else
                                        Storage.WavFile = null;
                                    break;

                                case "samplerate":
                                    settings.SampleRate = int.Parse(re.Value);
                                    break;

                                case "channels":
                                    settings.Channels = (Channels)int.Parse(re.Value);
                                    break;

                                case "bitspersample":
                                    settings.BitsPerSample = (BPS)int.Parse(re.Value);
                                    break;

                                case "overflow":
                                    settings.CutOverflow = bool.Parse(re.Value);
                                    break;
                            }
                            break;

                        // Bei einem Endtag, wird der zwischengespeicherte Wert gelöscht.
                        case XmlNodeType.EndElement:
                            currentElementName = "";
                            break;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Invalid config file!");
                Storage.Waves.Clear();
                Storage.Waves = null;
                settings = null;

                re.Close();
                fs.Close();
                return null;
            }

            // Der absolute Pfad der Projektdatei wird direkt in den Optionen gespeichert.
            settings.FileName = Path.GetFullPath(projectName);

            re.Close();
            fs.Close();
            return settings;
        }
    }
}
