﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WAVEing
{
    public partial class ProjectSettingsDialog : Form
    {
        public BPS BitsPerSample { get { return (BPS)bpsDropDown.SelectedItem; } }
        public Channels AudioChannels { get { return (Channels)channelDropDown.SelectedItem; } }
        public int SampleRate { get { return decimal.ToInt32(sampleRateNumeric.Value); } }
        public Formats Format { get { return (Formats)formatDropDown.SelectedItem; } }

        public string RawDataPath { get { return (string)dependencyTextBox.Tag; } }
        public bool CutOverflow { get { return overflowCheckbox.Checked; } }


        public ProjectSettingsDialog()
        {
            InitializeComponent();

            bool projectExists = GlobalSettings.CurrentProject != null;

            // Initialisiert die DropDownBoxen mit den Werten aus den Enumerationen und setzt die Startwerte für alle Boxen.
            foreach (BPS bps in Enum.GetValues(typeof(BPS)).Cast<BPS>())
                bpsDropDown.Items.Add(bps);
            bpsDropDown.SelectedItem = !projectExists ? GlobalSettings.DefaultBitsPerSample : GlobalSettings.CurrentProject.BitsPerSample;

            foreach (Channels ch in Enum.GetValues(typeof(Channels)).Cast<Channels>())
                channelDropDown.Items.Add(ch);
            channelDropDown.SelectedItem = !projectExists ? GlobalSettings.DefaultChannels : GlobalSettings.CurrentProject.Channels;

            foreach (Formats f in Enum.GetValues(typeof(Formats)).Cast<Formats>())
                formatDropDown.Items.Add(f);
            formatDropDown.SelectedItem = !projectExists ? GlobalSettings.DefaultFormat : GlobalSettings.CurrentProject.Format;

            sampleRateNumeric.Value = !projectExists ? GlobalSettings.DefaultSampleRate : GlobalSettings.CurrentProject.SampleRate;

            dependencyTextBox.Text = !projectExists ? "" : Storage.WavFile == null ? "" : Storage.WavFile.FileName;
            overflowCheckbox.Checked = !projectExists ? false : GlobalSettings.CurrentProject.CutOverflow;
        }

        private void dependencyButton_Click(object sender, EventArgs e)
        {
            // Versucht eine externe Audiodatei (.wav) einzubinden.
            OpenFileDialog od = new OpenFileDialog();

            od.Filter = "WAVE|*.wav";

            if (od.ShowDialog() == DialogResult.OK)
            {
                // Wenn eine Datei ausgewählt wurde, wird diese in der Textbox angezeigt.
                dependencyTextBox.Text = Path.GetFileName(od.FileName);
                dependencyTextBox.Tag = Path.GetFullPath(od.FileName);
            }
        }

        private void delDependency_Click(object sender, EventArgs e)
        {
            dependencyTextBox.Text = "";
            dependencyTextBox.Tag = "";
        }
    }
}
