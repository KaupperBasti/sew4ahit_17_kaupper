﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WAVEing
{
    /// <summary>
    /// Enthält die Daten eines WAV-Files
    /// </summary>
    public class RawData : ITimeValues
    {
        public string FileName { get; set; }
        private int channel;

        public RawData(string fileName)
        {
            FileName = fileName;
            channel = 1;
        }

        public void SetStandardChannel(int ch)
        {
            this.channel = ch;
        }

        /// <summary>
        /// Liest aus der Datei die Amplitude zu einem gegebenen Zeitpunkt aus.
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public double GetValueAt(double time)
        {
            if (FileName == null || FileName == "")
                return 0;
            FileStream fs = new FileStream(FileName, FileMode.Open);


            // Wichtige Einstellungen auslesen und in Integers konvertieren. (Offsets laut Doku!)
            byte[] buffer = new byte[4];
            fs.Position = 22;
            fs.Read(buffer, 0, 2);
            buffer.Reverse();
            int channels = BitConverter.ToInt32(buffer, 0);
           

            fs.Position = 34;
            fs.Read(buffer, 0, 2);
            buffer.Reverse();
            int bitsPerSample = BitConverter.ToInt32(buffer, 0);
           

            fs.Position = 24;
            fs.Read(buffer, 0, 4);
            buffer.Reverse();
            int sampleRate = BitConverter.ToInt32(buffer, 0);
           

            long sampleToRead = (long)((double)channels * ((double)sampleRate * ((double)time / 1000.0))) + (Math.Min(channels, this.channel) - 1);
            
            buffer = new byte[4];
            fs.Position = 44 + (int)(sampleToRead * bitsPerSample / 8);
            fs.Read(buffer, 0, bitsPerSample / 8);
            fs.Close();


            buffer.Reverse();



            int absAmplitude = BitConverter.ToInt32(buffer, 0);

            // Rückumwandlung des Zweierkomplements
            if(absAmplitude != 0 && absAmplitude >> (bitsPerSample - 1) == 1)
                absAmplitude -= (int)(Math.Pow(2, bitsPerSample));
            absAmplitude *= -1;


            return  (double) absAmplitude / Math.Pow(2, bitsPerSample - 1) * 100;
        }
    }
}
