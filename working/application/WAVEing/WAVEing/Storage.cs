﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WAVEing
{
    /// <summary>
    /// Speichert die Daten während der Laufzeit.
    /// </summary>
    class Storage
    {
        public static List<Wave> Waves = new List<Wave>();
        public static RawData WavFile = null;
    }
}
