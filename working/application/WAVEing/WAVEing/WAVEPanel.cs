﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WAVEing
{
    /// <summary>
    /// Wird benutzt um dem Graphen hoffentlich das Flackern zu nehmen!
    /// </summary>
    class WAVEPanel : Panel
    {
        public WAVEPanel() : base()
        {
            DoubleBuffered = true;
        }
    }
}
