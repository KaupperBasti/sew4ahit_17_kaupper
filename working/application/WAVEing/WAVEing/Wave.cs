﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WAVEing
{
    /// <summary>
    /// Repräsentiert eine Sinusschwingung mit ihren Eigenschaften.
    /// </summary>
    class Wave : ITimeValues
    {
        public string Name { get;  set; }
        public double Offset { get;  set; }
        public double Duration { get;  set; }
        public double Frequency { get;  set; }
        public double Amplitude { get;  set; }

        public Wave()
        {
            Name = "";
            Offset = 0;
            Duration = 0;
            Frequency = 0;
            Amplitude = 0;
        }

        public Wave(string name, double offset, double duration, double frequency, double amplitude)
        {
            Name = name;
            Offset = offset;
            Duration = duration;
            Frequency = frequency;
            Amplitude = amplitude;
        }

        /// <summary>
        /// Berechnung der Amplitude zu einer bestimmten Zeit.
        /// </summary>
        /// <param name="time">Zeit in Millisekunden.</param>
        /// <returns>Amplitude in % des Maximums.</returns>
        public double GetValueAt(double time)
        {
            if (Frequency <= 0 || Amplitude <= 0 || Duration <= 0)
                return 0;

            // Berechnet die Dauer bis zur nächsten Nullstelle, damit die Welle nicht zu abgehakt wird.
            double periodeTime = 1 / Frequency;
            double realDuration = Duration % (periodeTime * 500) == 0 ? Duration : Duration + periodeTime * 500 - Duration % (periodeTime * 500);

            // Außerhalb der gegebenen Zeit hat die Welle einen Ausschlag von 0.
            if (time < Offset || time > Offset + realDuration)
                return 0;

            return Amplitude * Math.Sin(2 * Math.PI * Frequency * (time - Offset) / 1000);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
