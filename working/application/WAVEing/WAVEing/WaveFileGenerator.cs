﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace WAVEing
{
    class WaveFileGenerator
    {

        #region Helpfunctions

        // Selbstbeschreibend!

        private static uint ConvertEndianness32(uint zahl)
        {
            return ((zahl & 0x000000FF) << (8 * 3)) + ((zahl & 0x0000FF00) << (8)) + ((zahl & 0x00FF0000) >> (8)) + ((zahl & 0xFF000000) >> (8 * 3));
        }

        private static uint ConvertEndianness16(uint zahl)
        {
            return ((zahl & 0x00FF) << (8)) + ((zahl & 0xFF00) >> (8));
        }

        private static byte[] GetBytes16(uint zahl)
        {
            List<byte> list = new List<byte>();
            list.Add((byte)((zahl & 0xff00)>>8));
            list.Add((byte)(zahl & 0x00ff));
            return list.ToArray();
        }

        private static byte[] GetBytes32(uint zahl)
        {
            List<byte> list = new List<byte>();
            list.Add((byte)((zahl & 0xff000000)>>24));
            list.Add((byte)((zahl & 0x00ff0000)>>16));
            list.Add((byte)((zahl & 0x0000ff00)>>8));
            list.Add((byte)(zahl & 0x000000ff));
            return list.ToArray();
        }

        private static uint StringToInt(string s)
        {
            if (s.Length > 4)
                return 0;

            uint ret = 0;
            for (int i = 0; i < s.Length; i++)
                ret += ((uint)(char)s[i]) << ((3-i) * 8);

            return ret;
        }

        #endregion


        // Abgeschlossener Export in %
        public static double Percentage = 0.0;

        /// <summary>
        /// Export eine Liste an Wellen in eine WAV Datei mit den gegebenen Einstellungen.
        /// </summary>
        /// <param name="fs">Stream zur Datei.</param>
        /// <param name="waves">Wellen, die exportiert werden sollen.</param>
        /// <param name="length">Die Anzahl der Millisekunden zum Exportieren.</param>
        /// <param name="settings">Die zu verwendenden Einstellungen.</param>
        /// <returns>True, wenn die Operation erfolgreich war, andernfalls false.</returns>
        public static bool SaveToFile(FileStream fs, List<ITimeValues> waves, long length, ProjectSettings settings)
        {
            bool success = false;
            Percentage = 0.0;

            try
            {
                //Generieren der Headerdaten
                uint sampleRate = (uint)settings.SampleRate;
                uint numChannels = (uint)settings.Channels;
                uint bitsPerSample = (uint)settings.BitsPerSample;

                long samples = (long)(sampleRate * ((double)length / 1000));

                uint chunkID;
                uint chunkSize;
                uint format;


                uint subChunk1ID;
                uint subChunk1Size;

                uint audioFormat = 1;

                uint byteRate = sampleRate * numChannels * bitsPerSample / 8;
                uint blockAlign = numChannels * bitsPerSample / 8;

                uint subChunk2ID;
                uint subChunk2Size = 0;

                subChunk2Size = (uint)(samples * bitsPerSample / 8 * numChannels);
                subChunk1Size = 16;
                chunkSize = 4 + 8 + subChunk1Size + 8 + subChunk2Size;


                chunkID = StringToInt("RIFF");
                format = StringToInt("WAVE");
                subChunk1ID = StringToInt("fmt ");
                subChunk2ID = StringToInt("data");

                chunkSize = ConvertEndianness32((uint)chunkSize);
                subChunk1Size = ConvertEndianness32((uint)subChunk1Size);
                audioFormat = ConvertEndianness16((uint)audioFormat);
                numChannels = ConvertEndianness16((uint)numChannels);
                sampleRate = ConvertEndianness32((uint)sampleRate);
                bitsPerSample = ConvertEndianness16((uint)bitsPerSample);
                byteRate = ConvertEndianness32((uint)byteRate);
                blockAlign = ConvertEndianness16((uint)blockAlign);
                subChunk2Size = ConvertEndianness32((uint)subChunk2Size);


                List<byte> headerBytes = new List<byte>();
                headerBytes.AddRange(GetBytes32(chunkID));
                headerBytes.AddRange(GetBytes32(chunkSize));
                headerBytes.AddRange(GetBytes32(format));

                headerBytes.AddRange(GetBytes32(subChunk1ID));
                headerBytes.AddRange(GetBytes32(subChunk1Size));
                headerBytes.AddRange(GetBytes16(audioFormat));
                headerBytes.AddRange(GetBytes16(numChannels));
                headerBytes.AddRange(GetBytes32(sampleRate));
                headerBytes.AddRange(GetBytes32(byteRate));
                headerBytes.AddRange(GetBytes16(blockAlign));
                headerBytes.AddRange(GetBytes16(bitsPerSample));

                headerBytes.AddRange(GetBytes32(subChunk2ID));
                headerBytes.AddRange(GetBytes32(subChunk2Size));



                //Amplituden für die gesamte Länge berechnen
                double max = 0;
                for (int i = 0; i < samples; i++)
                {
                    double nmax = 0;
                    foreach (ITimeValues v in waves)
                        nmax -= v.GetValueAt(((double)i / settings.SampleRate * 1000));
                    if (nmax > max)
                        max = nmax;

                    Percentage += 50.0 / samples;
                }

                //berechnen des Skalierungsfaktors!
                double verticalScaleFactor = 1;
                if (!settings.CutOverflow)
                    verticalScaleFactor = Math.Min(1 / (max / 100), 1);

                // Schreiben des Headers.
                foreach (byte b in headerBytes)
                    fs.WriteByte(b);

                for (int i = 0; i < samples; i++)
                {
                    // Berechnung der aktuellen Amplitude (ev. skaliert!).
                    double amp = 0;
                    foreach (ITimeValues v in waves)
                        amp -= v.GetValueAt(((double)i / settings.SampleRate * 1000));
                    int current = (int)((amp * verticalScaleFactor) / 100 * Math.Pow(2, (int)settings.BitsPerSample - 1));

                    // Aufteilung in Bytes, je nach Einstellung.
                    List<byte> bytes = new List<byte>();
                    for (int j = (int)settings.BitsPerSample; j > 0; j -= 8, current >>= 8)
                        bytes.Add((byte)(current & 0xff));

                    // Schreiben der Daten.
                    foreach (byte b in bytes)
                        for (int j = 0; j < (int)settings.Channels; j++)
                            fs.WriteByte(b);

                    Percentage += 50.0 / samples;
                }

                fs.Flush();
                success = true;
            }
            catch { success = false; }
            finally
            {
                fs.Close();
            }

            return success;
        }

        /// <summary>
        /// Erstellt anhand einer Datei ein 'RawData' Objekt inklusive Validierungen.
        /// </summary>
        /// <param name="fileName">Die Datei die verwendet werden soll.</param>
        /// <returns>RawData-Objekt, wenn die Operation erfolgreich war, andernfalls null.</returns>
        public static RawData LoadFromFile(string fileName)
        {
            // Existiert die Datei?
            if (!File.Exists(fileName))
                return null;

            // Richtige Dateiendung=
            if (Path.GetExtension(fileName) != ".wav")
                return null;


            FileStream fs = new FileStream(fileName, FileMode.Open);
            
            // Überprüfen markanter Bytes (siehe Doku!)
            byte[] buffer = new byte[4];
            fs.Read(buffer, 0, 4);

            if (Encoding.ASCII.GetString(buffer) != "RIFF")
            {
                fs.Close();
                return null;
            }

            fs.Position = 8;
            fs.Read(buffer, 0, 4);

            if (Encoding.ASCII.GetString(buffer) != "WAVE")
            {
                fs.Close();
                return null;
            }

            fs.Close();

            return new RawData(fileName);
        }
    }
}
