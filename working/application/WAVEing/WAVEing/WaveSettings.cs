﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WAVEing
{
    /// <summary>
    /// Repräsentiert die Standardeinstellung eines Audiofiles
    /// </summary>
    public class WaveSettings
    {
        public Channels Channels { get; set; }
        public BPS BitsPerSample { get; set; }
        public int SampleRate { get; set; }
        public Formats Format { get; set; }

        public WaveSettings()
        {
            Channels = WAVEing.Channels.Mono;
            BitsPerSample = BPS.Sechzehn;
            SampleRate = 44100;
            Format = Formats.Wave;
        }

        public WaveSettings(Channels chs, BPS bps, int sampleRate, Formats f):this()
        {
            Channels = chs;
            BitsPerSample = bps;
            SampleRate = sampleRate;
            Format = f;
        }
    }
}
