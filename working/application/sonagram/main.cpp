#include <stdio.h>
#include <malloc.h>
#include <cmath>
#include "fft.h"

using namespace std;

void padd(char c, float times)
{
    int i;
    for(i=0; i < times;i++)
        printf("%c", c);
}

float len(float data, int max)
{
    return max * abs(data);
}

int main()
{
    int samplespersecond = 65536;
    float seconds = 1;
    float frequency = 1;
    float* data = (float*)calloc(sizeof(float), samplespersecond * seconds);
    float* magnitude = (float*)calloc(sizeof(float), samplespersecond * seconds);
    float* phase = (float*)calloc(sizeof(float), samplespersecond * seconds);
    float* power = (float*)calloc(sizeof(float), samplespersecond * seconds);
    float* avg_power = (float*)calloc(sizeof(float), samplespersecond * seconds);

    int i=0;
    for(;i< samplespersecond * seconds; i++)
        data[i] = (sin(frequency * (i * 2 * M_PI / (float)(samplespersecond)))*1/10) + (sin(frequency * 10 * (i * 2 * M_PI / (float)(samplespersecond)))*1/3) + sin(frequency * 410 * (i * 2 * M_PI /(float)(samplespersecond)))*1/2;


    int width = 15;
    fft myfft;
    double windowtime = round(1.f *100)/100; //wie gro� ist das Zeitfenster?
    myfft.powerSpectrum(0, windowtime*samplespersecond*seconds/2, data, windowtime*samplespersecond, magnitude, phase, power, avg_power);

    int maxindex = 0;
    float max = 0;
    float powe = 0;

    for(i=0; i < samplespersecond * seconds;i++)
    {
        if(magnitude[i] > max)
        {
            max = magnitude[i];
            maxindex = i;
            powe = phase[i];
        }
    }
    maxindex/=windowtime;//Frequenz des st�rksten Signals
    printf("%dHz: %f:2, %f:2", maxindex, max, powe);

}


